using MEC;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class BenchmarkingTest : MonoBehaviour
{
    [SerializeField, ReadOnly] UniversalRenderPipelineAsset assetToChange => GraphicsSettings.currentRenderPipeline as UniversalRenderPipelineAsset;
    public BenchmarkSlider FPS;
    public BenchmarkSlider Render;
    public BenchmarkSlider Quality;

    public Button btnSave;
    public Button btnReset;
    public Button btnLoad;

    public GameObject settingParent;
    private BenchmarkSettings settings;
    const string SettingsKey = "settings.json";
    string SettingsFilePath => Path.Combine(Application.streamingAssetsPath, SettingsKey);

    public void Start()
    {
        btnReset.onClick.AddListener(OnClickReset);
        btnSave.onClick.AddListener(OnClickSave);
        btnLoad.onClick.AddListener(OnClickLoad);

        FPS.slider.onValueChanged.AddListener(OnFPSChanged);
        Render.slider.onValueChanged.AddListener(OnRenderChanged);
        Quality.slider.onValueChanged.AddListener(OnQualityChanged);
        Timing.RunCoroutine(LateStart());
    }

    IEnumerator<float> LateStart()
    {
        yield return Timing.WaitForSeconds(0.5f);
        OnClickLoad();
    }

    private void OnQualityChanged(float newValue)
    {
        settings.Quality = (int)newValue;
        var selected = (int)newValue;
        QualitySettings.SetQualityLevel(selected);
        Render.slider.value = assetToChange.renderScale;
    }

    bool disableInput;
    private void Update()
    {
        if (disableInput) return;
        if(Input.GetKey(KeyCode.O) && Input.GetKey(KeyCode.P))
        {
            settingParent.SetActive(!settingParent.activeSelf);
            Timing.RunCoroutine(Delay());
        }
    }

    IEnumerator<float> Delay()
    {
        disableInput = true;
        yield return Timing.WaitForSeconds(0.5f);
        disableInput = false;
    }

    private void OnRenderChanged(float newValue)
    {
        var val = Mathf.Clamp(newValue, 0.1f, 2);
        assetToChange.renderScale = val;
        settings.Render = val;
    }

    private void OnFPSChanged(float newValue)
    {
        settings.FPS = (int)newValue;
        Application.targetFrameRate = (int)newValue;
    }

    private void OnClickSave()
    {
#if UNITY_WEBGL
        string json = JsonConvert.SerializeObject(settings);

        PlayerPrefs.SetString(SettingsFilePath, json);
        PlayerPrefs.Save();
#else
        string json = JsonConvert.SerializeObject(settings);
        File.WriteAllText(SettingsFilePath, json);
#endif
    }

    private void OnClickReset()
    {
        settings = new BenchmarkSettings();
        ApplySetting();
    }

    public void ApplySetting()
    {
        FPS.slider.value = settings.FPS;
        Render.slider.value = settings.Render;
        Quality.slider.value = settings.Quality;
    }

    private void OnClickLoad()
    {
#if UNITY_WEBGL
        var setting = PlayerPrefs.GetString(SettingsFilePath, "");

        if(string.IsNullOrEmpty(setting))
        {
            settings = new BenchmarkSettings();
        }
        else
        {
            string json = File.ReadAllText(SettingsFilePath);
            settings = JsonConvert.DeserializeObject<BenchmarkSettings>(json);
        }

        ApplySetting();
#else
        if(!File.Exists(SettingsFilePath))
            settings = new BenchmarkSettings();
        else
        {
            string json = File.ReadAllText(SettingsFilePath);
            settings = JsonConvert.DeserializeObject<BenchmarkSettings>(json);
        }

        ApplySetting();
#endif
    }

}

[System.Serializable]
public class BenchmarkSettings
{
    public int FPS = 30;
    public float Render = 1;
    public int Quality = 0;
}

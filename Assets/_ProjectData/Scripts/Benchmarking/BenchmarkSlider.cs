using MEC;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BenchmarkSlider : MonoBehaviour
{
    public Slider slider;
    public TextMeshProUGUI valueText;

    private void Start()
    {
        Assign();
        Timing.RunCoroutine(Delay(0.5f));
    }

    IEnumerator<float> Delay(float delay)
    {
        yield return Timing.WaitForSeconds(delay);
        UpdateText(slider.value);
    }

    public void Assign()
    {
        slider.onValueChanged.AddListener(UpdateText);
    }
    public void UpdateText(float newValue)
    {
        valueText.text = newValue.ToString("F1");
    }
}
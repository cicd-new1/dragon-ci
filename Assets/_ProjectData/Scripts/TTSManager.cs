using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using Utilities.WebRequestRest;

public class TTSManager : Singleton<TTSManager>
{
    bool isInited;
    public List<CacheRequest> CacheRequests;
    string baseVoiceID = "alloy";
    private void Awake()
    {
        CacheRequests = new List<CacheRequest>();
        var data = PlayerPrefs.GetString("Cache");
        if (!string.IsNullOrEmpty(data))
        {
            CacheRequests = JsonConvert.DeserializeObject<List<CacheRequest>>(data);
        }
    }

    [Button]
    public async void TestDownload(string text)
    {
        await Speechify(text, saveToCache: true);
    }
    public async Task<AudioClip> Speechify(string text, string voiceID = "", bool saveToCache = false, CancellationToken cancellationToken = default)
    {
        var usedID = voiceID;
        if (!string.IsNullOrEmpty(voiceID))
        {
            usedID = baseVoiceID;
        }

        //Check Cache
        var clipGuid = $"{usedID}-{Guid.NewGuid()}".ToString();
        var fileName = $"{clipGuid}.mp3";
        var filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        var cacheAudio = await LoadClip(usedID, text);
        if (cacheAudio) return cacheAudio;
        TaskCompletionSource<AudioClip> taskCompletionSource = new TaskCompletionSource<AudioClip>();
        
        var request = new SpeechBackendTTSRequest
        {
            text = text,
            voice = "alloy"
        };

        string jsonData = JsonConvert.SerializeObject(request);
        var usedURL = "https://api-staging.bythen.ai/ms/speech/v1/speech";
        using (UnityWebRequest webRequest = UnityWebRequest.PostWwwForm(usedURL, jsonData))
        {
            Debug.Log("Requesting to : " + usedURL + " with params : " + jsonData);
            webRequest.SetRequestHeader("Content-Type", "audio/mpeg");
            webRequest.SetRequestHeader("Accept", "application/json");
            byte[] jsonBytes = System.Text.Encoding.UTF8.GetBytes(jsonData);
            webRequest.uploadHandler = new UploadHandlerRaw(jsonBytes);
            webRequest.disposeUploadHandlerOnDispose = true;
            webRequest.disposeDownloadHandlerOnDispose = true;
            var downloaderAudio = new DownloadHandlerAudioClip(usedURL, AudioType.MPEG);
            webRequest.downloadHandler = downloaderAudio;
            var asyncOperation = webRequest.SendWebRequest();
            while (!asyncOperation.isDone)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    webRequest.Abort();
                }
                await Task.Yield();
            }

            webRequest.uploadHandler.Dispose();
            if (webRequest.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("Error: " + webRequest.error);
                taskCompletionSource.SetException(new System.Exception("Error: " + webRequest.error));
                return null;
            }
            else
            {
                if (saveToCache)
                {
                    SaveToCache(webRequest.downloadHandler.data, filePath, () =>
                    {
                        CacheRequests.Add(new CacheRequest()
                        {
                            voiceID = usedID,
                            filePath = filePath,
                            message = text
                        });

                        PlayerPrefs.SetString("Cache", JsonConvert.SerializeObject(CacheRequests));
                        PlayerPrefs.Save();
                    });
                }
                Debug.Log(webRequest.downloadHandler.ToString());
                return downloaderAudio.audioClip;
            }
        }
    }

    public async Task<AudioClip> LoadClip(string id, string message)
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        return null;
#endif
        var cacheAudio = CacheRequests.Find(x => x.voiceID == id && x.message == message);
        if(cacheAudio != null)
        {
            if (File.Exists(cacheAudio.filePath))
            {
                var audio = await Rest.DownloadAudioClipAsync($"file://{cacheAudio.filePath}", AudioType.MPEG, parameters: null);
                return audio;
            }
        }
        return null;
    }

    public async void SaveToCache(byte[] audioClip, string filePath, Action OnSaved, CancellationToken token = default)
    {
        var responseStream = new MemoryStream(audioClip);

        try
        {
            var fileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write, FileShare.None);

            try
            {
                await responseStream.CopyToAsync(fileStream, token);
                await fileStream.FlushAsync(token);
                OnSaved?.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            finally
            {
                fileStream.Close();
                await fileStream.DisposeAsync();
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
        finally
        {
            await responseStream.DisposeAsync();
        }
    }

}

public class CacheRequest
{
    public string voiceID;
    public string message;
    public string filePath;
}

[Serializable]
public class VoiceRequest
{
    public string text;
    public string model_id;
    //public VoiceSettings voice_settings;
}
[System.Serializable]
public class TextToSpeechRequest
{
    public string model;
    public string input;
    public string voice;
}
[System.Serializable]
public class SpeechBackendTTSRequest
{
    public string text;
    public string voice;
}


[Serializable]
public class VoiceSettings
{
    public int stability;
    public int similarity_boost;
    public float style;
    public bool use_speaker_boost;
}
[Serializable]
public class Subscription
{
    public string tier;
    public int character_count;
    public int character_limit;
    public bool can_extend_character_limit;
    public bool allowed_to_extend_character_limit;
    public long next_character_count_reset_unix;
    public int voice_limit;
    public int max_voice_add_edits;
    public int voice_add_edit_counter;
    public int professional_voice_limit;
    public bool can_extend_voice_limit;
    public bool can_use_instant_voice_cloning;
    public bool can_use_professional_voice_cloning;
    public string currency;
    public string status;
}

[Serializable]
public class ApiResponse
{
    public Subscription subscription;
    public bool is_new_user;
    public string xi_api_key;
    public bool can_use_delayed_payment_methods;
}
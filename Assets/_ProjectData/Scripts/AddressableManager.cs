using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

public class AddressableManager : Singleton<AddressableManager>
{
    public AssetReference sceneToLoad;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        Addressables.InitializeAsync(true).Completed += Initialized;
    }

    private void Initialized(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> obj)
    {
        LoadScene();
    }

    public void LoadScene()
    {
        Addressables.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive).Completed += AddressableManager_Completed;
    }

    private void AddressableManager_Completed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> obj)
    {
        Debug.Log("Loaded.");
        SceneManager.UnloadSceneAsync(0); //Initialization is the only scene in BuildSettings, thus it has index 0
    }
}

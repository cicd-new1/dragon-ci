using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

public class VoiceCreator : MonoBehaviour
{
    public AudioSource globalSource;
    public Queue<VoiceCreationRequest> voiceChat = new Queue<VoiceCreationRequest>();

    public System.Action<string> OnTaskPlayed { get; set; }
    // Update is called once per frame
    void Update()
    {
        CreateVoice();
        GetNextVoice();
    }

    const float k_FixedUpdatePeriod = 0.1f;
    float m_CurrentFixedUpdateTime;
    public AudioSource AudioPlayer { get; set; }
    public Queue<VoiceCreationResult> voiceQueue = new Queue<VoiceCreationResult>();
    VoiceCreationResult current;

    private void Awake()
    {
        voiceQueue = new Queue<VoiceCreationResult>();
        voiceChat = new Queue<VoiceCreationRequest>();
    }

    public bool IsOtherPlaying(string you)
    {
        if (current == null) return false;
        if (globalSource.isPlaying/* && current.actor != you*/) return true;

        return false;
    }
    void GetNextVoice()
    {
        m_CurrentFixedUpdateTime += Time.deltaTime;
        if (m_CurrentFixedUpdateTime <= k_FixedUpdatePeriod) return;
        m_CurrentFixedUpdateTime = 0f;
        if (!AudioPlayer) return;
        if (AudioPlayer.isPlaying) return;
        if (!globalSource) return;
        if (globalSource.isPlaying) return;
        if (voiceQueue.Count == 0) return;
        if (voiceQueue.TryDequeue(out VoiceCreationResult currentVoice))
        {
            current = currentVoice;
            OnTaskPlayed?.Invoke(currentVoice.taskID);
            if (currentVoice.clip)
            {
                if(currentVoice.global)
                {
                    if (globalSource)
                    {
                        globalSource.volume = 1f;
                        globalSource.clip = currentVoice.clip;
                        globalSource.Play();
                    }
                }
                else
                {
                    if (AudioPlayer)
                    {
                        AudioPlayer.volume = 1f;
                        AudioPlayer.clip = currentVoice.clip;
                        AudioPlayer.Play();
                    }
                }
            }
        }
    }
    public bool isCreating { get; set; }
    public CancellationTokenSource cancellationTokenSource { get; set; }
    public async void CreateVoice()
    {
        if (isCreating) return;
        if (voiceChat.Count == 0) return;
        if (voiceChat.TryDequeue(out VoiceCreationRequest currentVoice))
        {
            if (!string.IsNullOrEmpty(currentVoice.message))
            {
                isCreating = true;
                cancellationTokenSource = new CancellationTokenSource();
                var result = await TTSManager.Instance.Speechify(currentVoice.message,
                    currentVoice.voiceID, currentVoice.cache, cancellationToken: cancellationTokenSource.Token);
                if(result != null)
                {
                    voiceQueue.Enqueue(new VoiceCreationResult()
                    {
                        taskID = currentVoice.taskID,
                        clip = result,
                        global = currentVoice.global,
                        actor = currentVoice.actor,
                    });
                }
                isCreating = false;
            }
        }
    }

    public void AddVoice(VoiceCreationRequest req)
    {
        voiceChat.Enqueue(req);
    }

    public void ForceStop()
    {
        globalSource.Stop();
        AudioPlayer.Stop();
        voiceChat = new Queue<VoiceCreationRequest>();
        voiceQueue = new Queue<VoiceCreationResult>();
        isCreating = false;
        cancellationTokenSource.Cancel();
    }
}

[System.Serializable]
public class VoiceCreationRequest
{
    //public bool useOpenAI;
    public string taskID;
    public string message;
    public string voiceID;
    public bool global;
    public bool cache;
    public string actor;
}
[System.Serializable]
public class VoiceCreationResult
{
    public string taskID;
    public AudioClip clip;
    public bool global;
    public string actor;
}
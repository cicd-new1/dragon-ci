using MEC;
using MintzuWorks;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;
using WebSocketSharp;

public class DemoWebsocketManager : MonoBehaviour
{
    public bool useWebsocket;

#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void SignalStartTalking();

    [DllImport("__Internal")]
    private static extern void SignalEndTalking();
    [DllImport("__Internal")]
    private static extern void SignalAppLoaded();

#endif
    private void Start()
    {
        voiceCreator.OnTaskPlayed += StartTalking;
        MessageEventArgs = new Queue<string>();
        InitializeCharacterRequirements();
        InitializeWebSocket();
        ConstantCheckWS();

#if UNITY_WEBGL && !UNITY_EDITOR
        WebGLInput.captureAllKeyboardInput = false;
#endif

        Timing.RunCoroutine(ConstantReset());
        //Cursor.visible = false;
        if (startingCharacter)
        {
            ApplyCurrentCharacter(startingCharacter);
        }

        //PreDownload();
    }

    void PreDownload()
    {
        // Download the asset and its dependencies asynchronously
        //Pre-download All Asset
        foreach (var character in ListOfCharacter)
        {
            AsyncOperationHandle handle = Addressables.DownloadDependenciesAsync(character.spawnedPrefab);
            handle.Completed += (a) => { Debug.Log(character.Name + " downloaded and ready to load."); };
        }
    }

    IEnumerator<float> ConstantReset()
    {
        while (true)
        {
            yield return Timing.WaitForSeconds(0.5f);
            logger.ResetPosition();
        }
    }

    private void OnDestroy()
    {
        voiceCreator.OnTaskPlayed -= StartTalking;
        DestroyWebSocket();
    }

    #region Character Spawning

    [Title("Character")]
    public CharacterData startingCharacter;
    public List<CharacterData> ListOfCharacter;
    public VoiceCreator voiceCreator;
    public Transform spawnPoint;
    public Transform cubeBackground;
    public Light mainLight;
    private Renderer cubeRenderer;

    private CharacterData currentData;
    private CharacterEntity currentCharacter;
    int voiceIndex;

    [Button]
    public void ChangeCharacter(int index)
    {
        ApplyCurrentCharacter(ListOfCharacter[index]);
    }
    public void InitializeCharacterRequirements()
    {
        cubeRenderer = cubeBackground.GetComponent<Renderer>();
    }

    private void ApplyCurrentCharacter(CharacterData characterData)
    {
        currentData = characterData;

        if (currentCharacter != null)
        {
            Destroy(currentCharacter.gameObject);
        }
        Addressables.InstantiateAsync(characterData.spawnedPrefab, spawnPoint.position, spawnPoint.rotation).Completed += CharacterSpawned;

        ChangeBackground();
    }

    private void CharacterSpawned(AsyncOperationHandle<GameObject> obj)
    {
        var spawned = obj.Result;
        var entity = spawned.GetComponent<CharacterEntity>();
        if (entity)
        {
            currentCharacter = entity;
            voiceCreator.AudioPlayer = currentCharacter.audioSource;
            currentCharacter.BodyControl.HandleHello();
        }
    }

    public void ChangeBackground()
    {
        if (cubeRenderer.sharedMaterial.HasProperty("_BaseMap"))
        {
            if (currentData.background)
            {
                cubeRenderer.sharedMaterial.SetTexture("_BaseMap", currentData.background);
            }
        }

        mainLight.color = currentData.lightColor;
    }

    #endregion

    #region WebSocket
    WebSocket ws;
    Queue<string> MessageEventArgs;
    bool connecting;
    CoroutineHandle connectHandler;
    private void InitializeWebSocket()
    {
        if (!useWebsocket) return;
        if (connecting) return;
        Timing.KillCoroutines(connectHandler);
        connectHandler = Timing.RunCoroutine(TryWSConnect());
    }

    public float timeout = 2f;
    bool appLoadedCalled { get; set; }
    IEnumerator<float> TryWSConnect()
    {
        var time = Time.time;
        connecting = true;
        ws = new WebSocket("ws://localhost:8080/ws?host=1");
        ws.ConnectAsync();

        LogToScreen("[WS] Connecting...");

        yield return Timing.WaitUntilTrue(() => ws.ReadyState == WebSocketState.Open || Time.time > time + timeout);

        if (ws.ReadyState == WebSocketState.Open)
        {
            LogToScreen("[WS] Connected.");

#if UNITY_WEBGL && !UNITY_EDITOR
            if (!appLoadedCalled)
            {
                SignalAppLoaded();
                appLoadedCalled = true;
            }
#endif
            ws.OnMessage += (sender, e) =>
            {
                if (!string.IsNullOrEmpty(e.Data))
                {
                    MessageEventArgs.Enqueue(e.Data);
                }
            };
        }
        else
        {
            LogToScreen("[WS] Can't connect.");
        }

        connecting = false;
    }

    private void Ws_OnClose(object sender, CloseEventArgs e)
    {
        LogToScreen("[WS] Connection Closed.");
        connecting = false;
    }

    private void Ws_OnError(object sender, ErrorEventArgs e)
    {
        LogToScreen("[WS] Error " + e.Message);
        connecting = false;
    }

    public void ConstantCheckWS()
    {
        if (!useWebsocket) return;

        Timing.RunCoroutine(KeepConnection());
    }
    public float timeCheck = 2f;
    IEnumerator<float> KeepConnection()
    {
        while (true)
        {
            yield return Timing.WaitForSeconds(timeCheck);

            if (!ws.IsAlive)
            {
                LogToScreen("[WS] Disconnected!");

                yield return Timing.WaitForOneFrame;
                yield return Timing.WaitForOneFrame;
                InitializeWebSocket();
            }
        }
    }

    //Tambah log.
    //Tambah always connected.

    public string eventClientTest;
    [Button]
    public void TestQueue()
    {
        QueueEvent(eventClientTest);
    }

    public void QueueEvent(string eventJSON)
    {
        Debug.Log("Receive event : " + eventJSON);
        if (string.IsNullOrEmpty(eventJSON)) return;
        MessageEventArgs.Enqueue(eventJSON);
    }

    private void Update()
    {
        if (MessageEventArgs.TryDequeue(out var e))
        {
            var generalData = JsonConvert.DeserializeObject<WS_General>(e);
            LogToScreen("[WS] Processing : " + generalData.eventName);
            if (!string.IsNullOrEmpty(generalData.eventName))
            {
                switch (generalData.eventName)
                {
                    case "app/client-connected":
                        OnClientConnected();
                        break;

                    case "app/start-recording":
                        OnStartRecording();
                        break;

                    case "app/chatbot-input-send":
                        OnReceiveUserInput();
                        break;

                    case "app/chatbot-voice-send":
                        OnReceiveVoice(e);
                        break;

                    case "app/recording-no-result":
                        OnSTTNoResult();
                        break;

                    case "app/chatbot-start-process":
                        OnStartProcessing();
                        break;

                    case "app/chatbot-end-process":
                        OnEndProcess();
                        break;

                    case "app/setting-change-character":
                        OnChangeCharacter(e);
                        break;

                    case "app/setting-change-mode":
                        OnChangeMode(e);
                        break;

                    case "app/setting-toggle-voice":
                        OnToggleVoice(e);
                        break;

                    case "app/stop-generate":
                        OnStopGenerate();
                        break;

                    case "app/toggle-debug":
                        OnToggleDebug();
                        break;

                    case "app/toggle-input":
                        OnToggleInput();
                        break;
                }
            }
        }
    }

    private void OnToggleInput()
    {
#if UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = !WebGLInput.captureAllKeyboardInput;
#endif
    }

    public VerticalLayoutGroup logger;
    public TextMeshProUGUI logUI;
    public Transform ScreenLog;
    public int max = 15;
    public void LogToScreen(string message)
    {
        if (logger.transform.childCount > max)
        {
            Destroy(logger.transform.GetChild(0).gameObject);
        }

        var logSpawned = Instantiate(logUI, logger.transform);
        logSpawned.text = message;
        logger.ResetPosition();
    }

    private void OnToggleDebug()
    {
        ScreenLog.gameObject.SetActive(!ScreenLog.gameObject.activeSelf);
    }
    private void OnClientConnected()
    {
        LogToScreen("Handle hello");
        if (!currentCharacter) return;
        currentCharacter.BodyControl.HandleHello();
    }

    private void OnStartRecording()
    {
        if (!currentCharacter) return;
        currentCharacter.BodyControl.HandleHearing();
    }


    private void OnReceiveUserInput()
    {
        //bool _ = false;
        //var req = JsonConvert.DeserializeObject<WS_UserInput>(e.Data);
        //if (req != null)
        //{
        //    if (!string.IsNullOrEmpty(req.data.message))
        //    {
        //        GenerateVoice(req.data.message, ref _);
        //    }
        //}
    }

    ChatBotEmotion currentEmotion;
    bool firstGenerated;
    private void OnReceiveVoice(string e)
    {
        var voiceRequest = JsonConvert.DeserializeObject<WS_SendVoice>(e);
        if (voiceRequest != null)
        {
            if (!string.IsNullOrEmpty(voiceRequest.data.emotion))
            {
                if (Enum.TryParse(voiceRequest.data.emotion, true, out currentEmotion))
                {
                    //LogToScreen("Current emotion : " + currentEmotion);
                }
            }

            if (!string.IsNullOrEmpty(voiceRequest.data.voice))
            {
                GenerateVoice(voiceRequest.data.voice, ref firstGenerated);
            }
        }
    }

    IEnumerator<float> CheckIsAIResponding()
    {
        while (voiceCreator.voiceQueue.Count > 0 || voiceCreator.voiceChat.Count > 0 || voiceCreator.AudioPlayer.isPlaying ||
                voiceCreator.globalSource.isPlaying || voiceCreator.isCreating)
        {
            yield return Timing.WaitForOneFrame;
        }
        EndTalking();
    }

    bool isTalking;
    CoroutineHandle waitTalking;
    private void StartTalking(string taskID)
    {
        if (currentTaskID != taskID) return;
        LogToScreen("Start talking : " + taskID);
        StartTalking();
    }
    private void StartTalking()
    {
        if (isTalking) return;
        Timing.KillCoroutines(waitTalking);
        waitTalking = Timing.RunCoroutine(WaitStartTalking());
    }

    IEnumerator<float> WaitStartTalking()
    {
        while (voiceCreator.IsOtherPlaying(currentData.id))
        {
            yield return Timing.WaitForOneFrame;
        }
        isTalking = true;
        currentCharacter.BodyControl.HandleTalking(currentEmotion);
        currentCharacter.HeadControl.HandleFacial(currentEmotion);
        OnStartTalking();
    }

    public void EndTalking()
    {
        isTalking = false;
        currentCharacter.BodyControl.HandleIdle();
        currentCharacter.HeadControl.HandleFacial(ChatBotEmotion.Neutral);
        OnEndTalking();
    }


    string currentTaskID = "LETS_PLAY";
    private void GenerateVoice(string temp, ref bool firstGenerated)
    {
        var req = new VoiceCreationRequest()
        {
            message = temp,
            voiceID = /*false ? currentData.voiceOpenaI : */currentData.voices.ElementAtOrDefault(voiceIndex),
            cache = true,
            actor = currentData.id
        };
        if (!firstGenerated)
        {
            currentTaskID = Guid.NewGuid().ToString();
            req.taskID = currentTaskID;
            firstGenerated = true;
        }
        voiceCreator.AddVoice(req);
    }


    private void OnSTTNoResult()
    {
        if (!currentCharacter) return;
        currentCharacter.BodyControl.HandleNoResult();
    }

    private void OnStartProcessing()
    {
        if (!currentCharacter) return;
        firstGenerated = false;
        currentCharacter.BodyControl.HandleThinking();
    }

    CoroutineHandle handler;
    private void OnEndProcess()
    {
        Timing.KillCoroutines(handler);
        handler = Timing.RunCoroutine(CheckIsAIResponding());
    }
    private void OnChangeCharacter(string e)
    {
        var req = JsonConvert.DeserializeObject<WS_ChangeCharacter>(e);
        if (req != null)
        {
            ChangeCharacter(req.data.index);
        }
    }
    private void OnChangeMode(string e)
    {
        var req = JsonConvert.DeserializeObject<WS_ChangeMode>(e);
        if (req != null)
        {
            currentCharacter.SetCamera(req.data.indexMode);
        }
    }
    private void OnToggleVoice(string e)
    {
        var req = JsonConvert.DeserializeObject<WS_ToggleVoice>(e);
        if (req != null)
        {

        }
    }

    private void OnStopGenerate()
    {
        isTalking = false;
        Timing.KillCoroutines(handler);
        EndTalking();
        voiceCreator.ForceStop();
        //tokenProcessStreamingChat.Cancel();
        Timing.KillCoroutines(waitTalking);
    }

    private void OnStartTalking()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        SignalStartTalking();
#endif

        if (!useWebsocket) return;
        WS_General wsData = new WS_General()
        {
            eventName = "app/start-talking"
        };
        ws.Send(JsonConvert.SerializeObject(wsData));
        LogToScreen("[WS] Send : " + wsData.eventName);
        //LogToScreen("Message Send : " + JsonConvert.SerializeObject(test));
    }

    uLipSync.uLipSyncBlendShape test;
    private void OnEndTalking()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        SignalEndTalking();
#endif
        if (!useWebsocket) return;
        WS_General wsData = new WS_General()
        {
            eventName = "app/end-talking"
        };

        LogToScreen("[WS] Send : " + wsData.eventName);
        ws.Send(JsonConvert.SerializeObject(wsData));
    }

    private void DestroyWebSocket()
    {
        if (ws != null)
        {
            ws.Close();
        }
    }
    #endregion
}

//public interface IWebsocketEvent
//{
//    public string Event { get; set; }
//}

//[System.Serializable]
//public class WS_ChangeMode : IWebsocketEvent
//{
//    [JsonProperty(PropertyName = "event")]
//    public string eventName;
//    public ChangeModeRequest data;

//    public string Event { get => eventName; set => eventName = value; }
//}

[System.Serializable]
public class WS_General
{
    [JsonProperty(PropertyName = "event")]
    public string eventName;
}

[System.Serializable]
public class WS_ChangeMode
{
    [JsonProperty(PropertyName = "event")]
    public string eventName;
    public ChangeModeRequest data;
}

[System.Serializable]
public class WS_ChangeCharacter
{
    [JsonProperty(PropertyName = "event")]
    public string eventName;
    public ChangeAvatarRequest data;
}
[System.Serializable]
public class WS_SendVoice
{
    [JsonProperty(PropertyName = "event")]
    public string eventName;
    public ChatbotVoiceRequest data;
}

[System.Serializable]
public class WS_UserInput
{
    [JsonProperty(PropertyName = "event")]
    public string eventName;
    public ChatbotUserInputRequest data;
}

[System.Serializable]
public class WS_ToggleVoice
{
    [JsonProperty(PropertyName = "event")]
    public string eventName;
    public ToggleVoiceRequest data;
}


[System.Serializable]
public class ChangeModeRequest
{
    public int indexMode;
}

[System.Serializable]
public class ChangeAvatarRequest
{
    public int index;
}

[System.Serializable]
public class ToggleVoiceRequest
{
    public bool isOn;
}

[System.Serializable]
public class ChatbotUserInputRequest
{
    public string message;
}

[System.Serializable]
public class ChatbotVoiceRequest
{
    public string voice;
    public string emotion;
}

public enum SpaffCode
{
    Neutral = 0,
    Disgust = 1,
    Contempt = 2,
    Belligerence = 3,
    Domineering = 4,
    Criticism = 5,
    Anger = 6,
    Tension = 7,
    TenseHumor = 8,
    Defensiveness = 9,
    Whining = 10,
    Sadness = 11,
    Stonewalling = 12,
    Interest = 13,
    Validation = 14,
    Affection = 15,
    Humor = 16,
    Surprise = 17,
    Joy = 18
}

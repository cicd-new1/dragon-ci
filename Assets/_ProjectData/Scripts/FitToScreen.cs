using UnityEngine;

public class FitToScreen : MonoBehaviour
{
    public float distanceFromCamera = 10f;
    Camera mainCamera;
    public float scaleXMultiplier = 1.0f;
    public float scaleYMultiplier = 1.0f;

    private void Awake()
    {
        mainCamera = Camera.main;
    }
    private void Update()
    {
        // Get the main camera
        if (mainCamera != null)
        {
            // Calculate the size of the cube
            float cubeSize = Mathf.Tan(mainCamera.fieldOfView * 0.5f * Mathf.Deg2Rad) * distanceFromCamera * 2f;

            // Adjust for screen aspect ratio
            float aspectRatio = Screen.width / (float)Screen.height;
            float horizontalSize = cubeSize * aspectRatio;

            // Apply the scale multipliers for X and Y
            float scaledHorizontalSize = horizontalSize * scaleXMultiplier;
            float scaledCubeSize = cubeSize * scaleYMultiplier;

            // Set the scale of the cube
            transform.localScale = new Vector3(scaledHorizontalSize, scaledCubeSize, 1f);

            // Position the cube just in front of the camera
            transform.position = mainCamera.transform.position + mainCamera.transform.forward * distanceFromCamera;
        }
    }
}

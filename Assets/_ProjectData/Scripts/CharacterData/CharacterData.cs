using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "New Character Data", menuName = "MintzuWorks/New Character Data...", order = 0)]
public class CharacterData : ScriptableObject
{
    public string id;
    public string Name;
    public string Description;
    public CharacterSource source;
    public AssetReferenceGameObject spawnedPrefab;
    public Sprite portrait;
    //public List<AvatarData> allAvatar;
    public List<string> voices;
    public string voiceOpenaI;
    public string narratorVoiceID;
    public string narratorVoiceOpenAIID;
    public Color colorCode;
    public Color lightColor;
    public Texture2D background;
}

public enum CharacterSource
{
    Tanooki,
    Azuki,
    CloneX
}

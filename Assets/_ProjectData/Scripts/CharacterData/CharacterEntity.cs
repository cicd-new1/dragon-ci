using Animancer;
using MEC;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEntity : MonoBehaviour
{
    public AnimancerComponent animancer;
    public AudioSource audioSource;
    public GameObject typingEmoji;
    //public AvatarDresser dresser;
    public InworldBasicHeadController HeadControl;
    public InworldBasicBodyController BodyControl;
    public InworldCustomEvent CustomEventListener;
    public List<GameObject> Cameras;

    [Title("Talking End")]
    public List<AudioClip> thinkingClip;
    public List<AudioClip> openAiThinkingClip;
    public float hummingChance = 50f;

    [Title("No Result")]
    public List<AudioClip> noResultClip;
    public List<AudioClip> openAiNoResultClip;
    public int reminderNoResultCycle = 5;
    private int currentReminderCycle;

    private void Awake()
    {
        foreach (var cam in Cameras)
        {
            if(cam) cam.transform.parent = null;
        }
    }
    public void SetCamera(int index)
    {
        foreach (var cam in Cameras)
        {
            cam.SetActive(false);
        }

        Cameras[index].SetActive(true);
    }
    public void SetTyping(bool isTyping)
    {
        if (typingEmoji) typingEmoji.SetActive(isTyping);
    }

    public bool TalkingEnded(bool isOpenAI)
    {
        bool hasSound = false;
        if(MintzuWorks.Utilities.Randomize(hummingChance))
        {
            if (MintzuWorks.Utilities.TryGetRandomFromList(isOpenAI ? openAiThinkingClip : thinkingClip, out var result))
            {
                hasSound = true;
                audioSource.clip = result;
                audioSource.Play();
            }
        }
        BodyControl.HandleThinking();

        return hasSound;
    }

    public bool NoResult(bool isOpenAI)
    {
        bool animationPlayed = false;
        if (currentReminderCycle % reminderNoResultCycle == 0)
        {
            if (MintzuWorks.Utilities.TryGetRandomFromList(isOpenAI ? openAiNoResultClip : noResultClip, out var result))
            {
                audioSource.clip = result;
                audioSource.Play();
            }

            animationPlayed = true;
        }
        currentReminderCycle++;
        BodyControl.HandleNoResult();

        return animationPlayed;
    }

}

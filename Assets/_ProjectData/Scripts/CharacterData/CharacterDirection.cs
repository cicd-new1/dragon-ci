using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDirection : MonoBehaviour
{
    private Transform cameraTransform;
    public bool followCam = true;
    public bool followMouse = true;
    private void Start()
    {
        // Find the main camera in the scene and store its transform
        cameraTransform = Camera.main.transform;

        if(Application.isMobilePlatform)
        {
            followMouse = false;
        }
    }

    private void Update()
    {
        if(followCam)
        {
            if (cameraTransform != null)
            {
                // Calculate the new rotation for the object
                Vector3 lookAtPos = new Vector3(cameraTransform.position.x, transform.position.y, cameraTransform.position.z);
                transform.LookAt(lookAtPos, Vector3.up);
            }
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MagicaCloth;
public class MagicaAttachChild : MonoBehaviour
{
    public List<MagicaAvatarParts> parts;
    MagicaAvatar avatar;
    private void Awake()
    {
        avatar = GetComponent<MagicaAvatar>();
        foreach (var item in parts)
        {
            avatar.AttachAvatarParts(item.gameObject);
        }
    }

}

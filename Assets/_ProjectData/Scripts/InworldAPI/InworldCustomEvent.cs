using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InworldCustomEvent : MonoBehaviour
{
    public Animator animator;

    public List<ExternalCustomEvent> externalEventData;
    public void CustomEventTriggered(string customEvent)
    {
        switch(customEvent)
        {
            case "animate_dance":
                animator.SetTrigger("Dance");
                break;
            case "animate_sleepy":
                animator.SetTrigger("Sleepy");
                break;
        }

        if (externalEventData == null) return;
        if (externalEventData.Count == 0) return;
        var index = externalEventData.FindIndex(x => x.customEvent == customEvent);
        if (index == -1) return;

        externalEventData[index].publicEvent?.Invoke();
    }
}

[System.Serializable]
public class ExternalCustomEvent
{
    public string customEvent;
    public UnityEvent publicEvent;
}
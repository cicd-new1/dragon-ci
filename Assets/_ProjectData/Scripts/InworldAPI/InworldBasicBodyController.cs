/*************************************************************************************************
* Copyright 2022 Theai, Inc. (DBA Inworld)
*
* Use of this source code is governed by the Inworld.ai Software Development Kit License Agreement
* that can be found in the LICENSE.md file or at https://www.inworld.ai/sdk-license
*************************************************************************************************/
using Animancer;
using Inworld.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// </summary>
public class InworldBasicBodyController : MonoBehaviour
{

    AnimancerComponent animancer;
    public BodyAnimationData hello;
    public BodyAnimationData goodbye;
    public BodyAnimationData idle;
    public BodyAnimationData talking;
    public BodyAnimationData hearing;
    public BodyAnimationData thinking;
    public BodyAnimationData noResult;

    public void PlayAnimation(ClipTransition clip)
    {
        PlayAnimation(clip, OnEndAnimation);
    }

    public void PlayAnimation(ClipTransition clip, Action OnEnd)
    {
        if (!clip.Clip.isLooping)
            clip.Events.OnEnd = OnEnd;
        animancer.Play(clip, 0.2f, mode: FadeMode.NormalizedFromStart);
    }
    public void OnEndAnimation()
    {
        HandleIdle(currentEmotion);
        played = new HashSet<int>();
    }

    public void HandleMainStatus(AnimMainStatus status)
    {
        Animator.SetInteger(s_Motion, (int)status);
    }
    HashSet<int> played;
    public void HandleTalking(ChatBotEmotion emotion)
    {
        currentEmotion = emotion;
        var targetEmo = talking.animations.Find(emo => emo.key.Contains(emotion));
        if(targetEmo != null)
        {
            if (targetEmo.weightedClips != null)
            {
                if (played.Count == targetEmo.weightedClips.Count) played = new HashSet<int>();
                if(MintzuWorks.Utilities.TryGetRandomFromList(targetEmo.weightedClips, played, out var selectedClip, out var index))
                {
                    played.Add(index);
                    if (selectedClip.clip.Clip != null)
                    {
                        PlayAnimation(selectedClip.clip, () => HandleTalking(currentEmotion));
                    }
                }
            }
        }
    }
    public void HandleEmotion(SpaffCode spaffCode)
    {
    }
    public bool Init()
    {
        Animator ??= GetComponentInChildren<Animator>();
        animancer = GetComponentInChildren<AnimancerComponent>();
        return Animator;
    }

    #region Private Variables
    static readonly int s_Emotion = Animator.StringToHash("Emotion");
    static readonly int s_Gesture = Animator.StringToHash("Gesture");
    static readonly int s_RemainSec = Animator.StringToHash("RemainSec");
    static readonly int s_Random = Animator.StringToHash("Random");
    static readonly int s_Motion = Animator.StringToHash("MainStatus");
    #endregion

    #region Properties
    public Animator Animator { get; set; }
    #endregion

    #region Monobehavior Functions
    void Awake()
    {
        enabled = Init();
        animancer = GetComponent<AnimancerComponent>();
        played = new HashSet<int>();
    }
    #endregion

    #region Callbacks
    void OnStatusChanged(ControllerStates newStatus)
    {
        if (newStatus == ControllerStates.Connected)
            HandleMainStatus(AnimMainStatus.Neutral);
    }
    public void OnAudioStarted()
    {
        //HandleMainStatus(AnimMainStatus.Talking);
    }

    public void OnAudioFinished()
    {
        //HandleMainStatus(AnimMainStatus.Neutral);
    }

    public void HandleGoodbye()
    {
        var bodyClip = goodbye.animations.FirstOrDefault();
        if(MintzuWorks.Utilities.TryGetRandomFromList(bodyClip.weightedClips, out var selected))
        {
            PlayAnimation(selected.clip);
        }
    }

    public void HandleHello()
    {
        var bodyClip = hello.animations.FirstOrDefault();
        if (MintzuWorks.Utilities.TryGetRandomFromList(bodyClip.weightedClips, out var selected))
        {
            PlayAnimation(selected.clip);
        }
    }

    public void HandleHearing()
    {
        var bodyClip = hearing.animations.FirstOrDefault();
        if (MintzuWorks.Utilities.TryGetRandomFromList(bodyClip.weightedClips, out var selected))
        {
            PlayAnimation(selected.clip);
        }
    }

    public void HandleThinking()
    {
        var bodyClip = thinking.animations.FirstOrDefault();
        if (MintzuWorks.Utilities.TryGetRandomFromList(bodyClip.weightedClips, out var selected))
        {
            PlayAnimation(selected.clip);
        }
    }
    public void HandleNoResult()
    {
        var bodyClip = noResult.animations.FirstOrDefault();
        if (MintzuWorks.Utilities.TryGetRandomFromList(bodyClip.weightedClips, out var selected))
        {
            PlayAnimation(selected.clip);
        }
    }
    ChatBotEmotion currentEmotion;
    public void HandleIdle()
    {
        HandleIdle(currentEmotion);
    }
    public void HandleIdle(ChatBotEmotion emotion)
    {
        var targetEmo = idle.animations.Find(emo => emo.key.Contains(emotion));
        if (targetEmo != null)
        {
            if (targetEmo.weightedClips != null)
            {
                if (MintzuWorks.Utilities.TryGetRandomFromList(targetEmo.weightedClips, out var selectedClip))
                {
                    if (selectedClip.clip.Clip != null)
                    {
                        PlayAnimation(selectedClip.clip);
                    }
                }
            }
        }
        else
        {
            var bodyClip = idle.animations.FirstOrDefault();
            if (MintzuWorks.Utilities.TryGetRandomFromList(bodyClip.weightedClips, out var selected))
            {
                PlayAnimation(selected.clip);
            }
        }
    }
    #endregion
}

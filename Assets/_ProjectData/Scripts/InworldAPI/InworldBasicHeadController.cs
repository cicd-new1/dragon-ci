using DG.Tweening;
using Inworld.Util;
using MintzuWorks;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
public class InworldBasicHeadController : MonoBehaviour
{
    #region Inspector Variables
    public FacialAnimationData facialData;
    public float m_MorphTime = 0.5f;
    #endregion

    #region Private Variables
    Transform m_trLookAt;
    Transform m_Transform;
    Vector3 m_vecInitPosition;
    Vector3 m_vecInitEuler;
    public SkinnedMeshRenderer faceSkin;
    FacialEmotion m_LastFacial;
    FacialEmotion m_CurrentFacial;
    float m_LookAtWeight;
    #endregion

    #region Properties
    /// <summary>
    ///     Get/Set the attached Animator.
    /// </summary>
    public Animator Animator { get; set; }
    #endregion

    #region Monobehavior Functions
    void Awake()
    {
        enabled = Init();
    }

    void OnEnable()
    {
        m_Transform = transform;
        m_vecInitEuler = m_Transform.localEulerAngles;
        m_vecInitPosition = m_Transform.localPosition;
    }
    void OnAnimatorIK(int layerIndex)
    {
        if (!Animator)
            return;
        if (m_trLookAt == null)
        {
            _StopLookAt();
            return;
        }
        _StartLookAt(m_trLookAt.position);
    }
    #endregion

    #region Private Functions
    void _StartLookAt(Vector3 lookPos)
    {
        m_LookAtWeight = Mathf.Clamp(m_LookAtWeight + 0.01f, 0, 1);
        Animator.SetLookAtWeight(m_LookAtWeight);
        Animator.SetLookAtPosition(lookPos);
    }
    void _StopLookAt()
    {
        m_Transform.localPosition = m_vecInitPosition;
        m_Transform.localEulerAngles = m_vecInitEuler;
        m_LookAtWeight = Mathf.Clamp(m_LookAtWeight - 0.01f, 0, 1);
        Animator.SetLookAtWeight(m_LookAtWeight);
    }
    #endregion

    #region Interface Implementation
    public void HandleMainStatus(AnimMainStatus status)
    {
        //Implement your own logic here.
    }
    ChatBotEmotion currentEmotion;
    public void HandleFacial(ChatBotEmotion emotion)
    {
        if (currentEmotion == emotion) return;
        currentEmotion = emotion;
        _ProcessEmotion(emotion);
    }
    public void HandleEmotion(SpaffCode spaffCode)
    {
        switch (spaffCode)
        {
        }
    }
    public bool Init()
    {
        Animator ??= GetComponentInChildren<Animator>();
        faceSkin ??= transform.GetComponentInChildren<SkinnedMeshRenderer>();
        return Animator /*&& m_Skin*/;
    }
    void _ProcessEmotion(ChatBotEmotion emotion)
    {
        FacialEmotion targetEmo = facialData.emotions.Find(emo => emo.emotion.Contains(emotion));
        if (targetEmo != null)
        {
            if (faceSkin == null) return;

            foreach (var setting in targetEmo.facialSetup)
            {
                var index = faceSkin.GetBlendShapeIndex(setting.shapeKey);
                if (index == -1) continue;
                float startWeight = faceSkin.GetBlendShapeWeight(index);

                if (Application.isPlaying)
                {
                    DOTween.To(() => startWeight, (value) =>
                    {
                        faceSkin.SetBlendShapeWeight(index, value);
                    }, setting.value, m_MorphTime);
                }
                else
                {
                    faceSkin.SetBlendShapeWeight(index, setting.value);
                }
            }
            //Debug.Log(targetEmo.emotion[0].ToString());
            //if(m_LastFacial != null) _ResetLastEmo(m_LastFacial);
            //m_LastFacial = m_CurrentFacial;
            //m_CurrentFacial = targetEmo;
            //StartCoroutine(_MorphFacial());
        }
    }
    void _ResetLastEmo(FacialEmotion emo)
    {
        if (!faceSkin || emo == null)
            return;
        for (int i = 0; i < faceSkin.sharedMesh.blendShapeCount; i++)
        {
            string currIterName = faceSkin.sharedMesh.GetBlendShapeName(i);
            var lastState = emo.facialSetup.FirstOrDefault(morph => morph.shapeKey == currIterName);
            if (lastState != null)
                faceSkin.SetBlendShapeWeight(i, 0);
        }
    }
    IEnumerator _MorphFacial()
    {
        if (!faceSkin)
            yield break;
        float morphTime = 0;
        while (morphTime < m_MorphTime)
        {
            for (int i = 0; i < faceSkin.sharedMesh.blendShapeCount; i++)
            {
                string currIterName = faceSkin.sharedMesh.GetBlendShapeName(i);
                float fCurrShapeWeight = faceSkin.GetBlendShapeWeight(i);
                try
                {
                    var lastState = m_LastFacial.facialSetup.FirstOrDefault(morph => morph.shapeKey == currIterName);
                    var currState = m_CurrentFacial.facialSetup.FirstOrDefault(morph => morph.shapeKey == currIterName);
                    // 1. Reset Old
                    if (lastState != null && currState == null)
                        faceSkin.SetBlendShapeWeight(i, Mathf.Lerp(fCurrShapeWeight, 0, 0.15f));
                    // 2. Apply New
                    if (currState != null)
                        faceSkin.SetBlendShapeWeight(i, Mathf.Lerp(fCurrShapeWeight, currState.value, 0.15f));
                }
                catch
                {
                    //Debug.LogError("Please check your InworldBasicHeadController Morph.");
                }
            }
            morphTime += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
    }
    #endregion
}

public interface InworldAnimation
{
    public Animator Animator { get; set; }
    public bool Init();
    public void HandleMainStatus(AnimMainStatus status);
    public void HandleEmotion(SpaffCode spaffCode);
}
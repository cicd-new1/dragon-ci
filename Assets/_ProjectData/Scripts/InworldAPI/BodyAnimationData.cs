using Animancer;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Body Animation Data", menuName = "MintzuWorks/Body Animation")]
public class BodyAnimationData : ScriptableObject
{
    public List<BodyClip> animations;
}

[System.Serializable]
public class BodyClip
{
    public List<ChatBotEmotion> key;
    //public List<ClipTransition> clip;
    public List<WeightedClipTransition> weightedClips;

    //[Button]
    //public void MigrateFromClip()
    //{
    //    weightedClips = new List<WeightedClipTransition>();
    //    foreach (var c in clip)
    //    {
    //        weightedClips.Add(new WeightedClipTransition()
    //        {
    //            clip = c,
    //            weight = 1
    //        });
    //    }
    //}
}

[System.Serializable]
public class WeightedClipTransition : IWeighted
{
    public float weight = 1;
    public ClipTransition clip;

    public float Weight { get => weight; set => weight = value; }
}

public interface IWeighted
{
    public float Weight { get; set; }
}
public enum ChatBotEmotion
{
    Neutral,
    Joy, 
    Sad,
    Angry
}

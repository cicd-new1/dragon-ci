using MintzuWorks;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "New Facial Animation Data", menuName = "MintzuWorks/Facial Animation")]
public class FacialAnimationData : ScriptableObject
{
    public SkinnedMeshRenderer face;
    public List<FacialEmotion> emotions;

    [Button]
    public void Inject()
    {
        foreach (var emot in emotions)
        {
            emot.Inject(face);
        }
    }
}

[System.Serializable]
public class FacialEmotion
{
    public List<ChatBotEmotion> emotion;
    public List<FacialSetup> facialSetup;
    public void Inject(SkinnedMeshRenderer renderer)
    {
        face = renderer;
        GetAllShape();
    }

    SkinnedMeshRenderer face;

    [Button]
    public void CopyFromCurrentSkin()
    {
        CopyFromCurrentSkin(false);
    }
    [Button]
    public void CopyAllFromCurrentSkin()
    {
        CopyFromCurrentSkin(true);
    }
    public void CopyFromCurrentSkin(bool includeZero)
    {
        facialSetup = new List<FacialSetup>();
        for (int i = 0; i < face.sharedMesh.blendShapeCount; i++)
        {
            var name = face.sharedMesh.GetBlendShapeName(i);

            if (string.IsNullOrEmpty(name)) continue;

            var setting = new FacialSetup();
            var index = face.GetBlendShapeIndex(name);
            var weight = face.GetBlendShapeWeight(index);

            if (weight != 0 || includeZero)
            {
                setting.shapeKey = name;
                setting.value = weight;
                facialSetup.Add(setting);
            }
        }
    }

    [Button]
    public void ActivateShape()
    {
        if (face == null) return;
        foreach (var facial in facialSetup)
        {
            var index = face.GetBlendShapeIndex(facial.shapeKey);
            if(index  != -1)
            {
                face.SetBlendShapeWeight(index, facial.value);
            }
        }
    }
    private void GetAllShape()
    {
        if (face == null) return;

        foreach (var item in facialSetup)
        {
            item.AllShapeName = new List<string>();
            for (int i = 0; i < face.sharedMesh.blendShapeCount; i++)
            {
                item.AllShapeName.Add(face.sharedMesh.GetBlendShapeName(i));
            }
        }
    }
}

[System.Serializable]
public class FacialSetup
{
    [ValueDropdown(nameof(AllShapeName)), HideLabel]
    public string shapeKey;
#if UNITY_EDITOR
    [CustomValueDrawer(nameof(MyStaticCustomDrawerStatic)), HorizontalGroup("a")]
#endif
    public float value;
    public List<string> AllShapeName { get; set; }

#if UNITY_EDITOR
    private static float MyStaticCustomDrawerStatic(float value)
    {
        return EditorGUILayout.Slider(value, 0f, 100f);
    }
#endif
}
using DG.Tweening;
using MEC;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace MintzuWorks
{

    public static class Utilities
    {
        public static void ResetPosition(this LayoutGroup parent)
        {
            Timing.RunCoroutine(RecalculatePosition(parent), Segment.LateUpdate);
        }

        public static IEnumerator<float> RecalculatePosition(this LayoutGroup parent)
        {
            var contentSize = parent.GetComponent<ContentSizeFitter>();
            if (contentSize)
            {
                contentSize.enabled = false;
                yield return Timing.WaitForOneFrame;
                contentSize.enabled = true;
            }
            parent.enabled = false;
            yield return Timing.WaitForOneFrame;
            parent.enabled = true;
        }
        public static int GetMinute(float second) => Mathf.FloorToInt(second / 60f);
        public static int GetSecond(float second) => Mathf.FloorToInt(second % 60f);
        public static bool IsPositionInsideView(Camera cam, Vector2 position)
        {
            if (!cam) return false;
            Vector3 viewportPoint = cam.WorldToViewportPoint(position);

            // Check if the point is inside the viewport
            bool isInsideView = (viewportPoint.x > 0 && viewportPoint.x < 1 && viewportPoint.y > 0 && viewportPoint.y < 1);

            return isInsideView;
        }

        public static Vector3 GetRandomOuterPosition(float addedOffset, Camera mainCamera)
        {
            if (!mainCamera) return new Vector3();

            var vertical = GetRandomVerticalOuterPosition(addedOffset, mainCamera);
            var horizontal = GetRandomHorizontalOuterPosition(addedOffset, mainCamera);
            var useVertical = UnityEngine.Random.Range(0, 2) == 0;
            var position = (useVertical ? vertical : horizontal);

            return position;
        }

        public static Vector3 GetRandomVerticalOuterPosition(float addedOffset, Camera mainCamera)
        {
            if (!mainCamera) return new Vector3();
            float width = mainCamera.orthographicSize * mainCamera.aspect + 1;
            float height = mainCamera.orthographicSize + 1;
            bool isFlip = UnityEngine.Random.Range(0, 2) == 0;
            var vertical = new Vector3(
                    mainCamera.transform.position.x + UnityEngine.Random.Range(-width, width),
                    mainCamera.transform.position.y + ((height + Utilities.GetRandom(0, addedOffset)) * (isFlip ? 1 : -1)),
                    1f);
            var position = vertical;

            return position;
        }

        public static void DestroyAllChild(Transform transform)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
                UnityEngine.Object.Destroy(transform.GetChild(i).gameObject);
        }
        public static Vector3 GetRandomHorizontalOuterPosition(float addedOffset, Camera mainCamera)
        {
            if (!mainCamera) return new Vector3();

            float width = mainCamera.orthographicSize * mainCamera.aspect + 1;
            float height = mainCamera.orthographicSize + 1;

            bool isFlip = UnityEngine.Random.Range(0, 2) == 0;

            var horizontal = new Vector3(
                    mainCamera.transform.position.x + ((width + Utilities.GetRandom(0, addedOffset)) * (isFlip ? 1 : -1)),
                    mainCamera.transform.position.y + UnityEngine.Random.Range(-height, height),
                    1f);

            var position = horizontal;

            return position;
        }



        public static void DrawCircle(Vector3 position, float radius, Color color, float duration)
        {
            // If either radius or number of segments are less or equal to 0, skip drawing
            if (radius <= 0.0f)
            {
                return;
            }

            // Single segment of the circle covers (360 / number of segments) degrees
            float angleStep = (360.0f / 32);

            // Result is multiplied by Mathf.Deg2Rad constant which transforms degrees to radians
            // which are required by Unity's Mathf class trigonometry methods

            angleStep *= Mathf.Deg2Rad;

            // lineStart and lineEnd variables are declared outside of the following for loop
            Vector3 lineStart = Vector3.zero;
            Vector3 lineEnd = Vector3.zero;

            for (int i = 0; i < 32; i++)
            {
                // Line start is defined as starting angle of the current segment (i)
                lineStart.x = Mathf.Cos(angleStep * i);
                lineStart.y = Mathf.Sin(angleStep * i);

                // Line end is defined by the angle of the next segment (i+1)
                lineEnd.x = Mathf.Cos(angleStep * (i + 1));
                lineEnd.y = Mathf.Sin(angleStep * (i + 1));

                // Results are multiplied so they match the desired radius
                lineStart *= radius;
                lineEnd *= radius;

                // Results are offset by the desired position/origin 
                lineStart += position;
                lineEnd += position;

                // Points are connected using DrawLine method and using the passed color
                Debug.DrawLine(lineStart, lineEnd, color, duration);
            }
        }
        public static bool IsHigher<T>(T myVal, T higherThan)
        {
            Type type = typeof(T);

            bool result = false;
            if (type.IsEnum)
            {
                try
                {
                    int val1 = Convert.ToInt32(myVal);
                    int val2 = Convert.ToInt32(higherThan);

                    return val1 > val2;
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
        public static bool IsMaxed<T>(T myVal)
        {
            Type type = typeof(T);

            bool result = false;
            if (type.IsEnum)
            {
                try
                {
                    int val1 = Convert.ToInt32(myVal);
                    return val1 >= System.Enum.GetValues(type).Length - 1;
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
        public static string GenerateStringFromGuid(int length)
        {
            var randomGuid = Guid.NewGuid().ToString();
            return randomGuid.Substring(0, length);
        }

        public static float GetRandom(float from, float to)
        {
            return UnityEngine.Random.Range(from, to);
        }

        public static T CreateObject<T>(T original)
        {
            // Create a new instance of the same type as the original object
            T copy = (T)Activator.CreateInstance(original.GetType());
            return copy;
        }
        public static T CopyObject<T>(T original)
        {
            // Create a new instance of the same type as the original object
            T copy = (T)Activator.CreateInstance(original.GetType());

            // Get all the fields of the object
            FieldInfo[] fields = copy.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            // Copy the values of the fields from the original object to the copy
            foreach (FieldInfo field in fields)
            {
                object value = field.GetValue(original);
                field.SetValue(copy, value);
            }

            return copy;
        }
        public static string RankPlayer(int rank)
        {
            if (rank % 10 == 1 && rank != 11)
            {
                return rank + "st";
            }
            else if (rank % 10 == 2 && rank != 12)
            {
                return rank + "nd";
            }
            else if (rank % 10 == 3 && rank != 13)
            {
                return rank + "rd";
            }
            else
            {
                return rank + "th";
            }
        }

        public static int GetLayerNumber(this LayerMask mask)
        {
            int value = mask.value;
            if (value == 0) return 0;  // Early out
            for (int l = 1; l < 32; l++)
                if ((value & (1 << l)) != 0) return l;  // Bitwise
            return -1;  // This line won't ever be reached but the compiler needs it
        }
        public static bool OutOfIndex(int count, int index)
        {
            if (index >= count) return true;
            else if (index < 0) return true;
            else return false;
        }

        public static bool TryGetRandomFromList<T>(List<T> list, out T result)
        {
            return TryGetRandomFromList<T>(list, new HashSet<int>(), out result, out _);
        }
        public static bool TryGetRandomFromList<T>(List<T> list, HashSet<int> excludes, out T result, out int index)
        {
            result = default(T);
            index = -1;
            List<T> nonNullElements = new List<T>();
            for (int i = 0; i < list.Count; i++)
            {
                if (excludes.Contains(i)) continue;
                var element = list[i];
                if (element != null)
                {
                    nonNullElements.Add(element);
                }
            }

            if (nonNullElements.Count > 0)
            {
                var check = nonNullElements.Where(item => item is IWeighted).ToList();
                bool isWeighted = check.Count > 0;

                if(isWeighted)
                {
                    List<IWeighted> weightedItems = nonNullElements.OfType<IWeighted>().ToList();
                    float totalWeight = weightedItems.Sum(item => item.Weight);
                    float randomValue = UnityEngine.Random.Range(0f, totalWeight);
                    float cumulativeWeight = 0f;
                    for (int i = 0; i < weightedItems.Count; i++)
                    {
                        cumulativeWeight += weightedItems[i].Weight;
                        if (randomValue <= cumulativeWeight)
                        {
                            result = (T)weightedItems[i];
                            index = list.IndexOf(result);
                            return true;
                        }
                    }

                    return false;
                }
                else
                {
                    int randomIndex = UnityEngine.Random.Range(0, nonNullElements.Count);
                    index = randomIndex;
                    result = nonNullElements[randomIndex];

                    if (result != null)
                        return true;
                    else
                        return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static int GetBlendShapeIndex(this SkinnedMeshRenderer skinnedMesh, string shapeName)
        {
            if (skinnedMesh == null) return -1;
            for (int i = 0; i < skinnedMesh.sharedMesh.blendShapeCount; i++)
            {
                if (skinnedMesh.sharedMesh.GetBlendShapeName(i) == shapeName)
                {
                    return i;
                }
            }
            return -1;
        }
        public static bool Randomize(float applyChance, float from = 0f, float to = 100f) => applyChance >= UnityEngine.Random.Range(from, to);

        public static bool IsInRange(this Vector2 vector, float value)
        {
            return value >= vector.x && value <= vector.y;
        }
        public static bool IsInRange(this Vector2Int vector, int value)
        {
            return value >= vector.x && value <= vector.y;
        }


        public static void PlayReverse(CanvasGroup parent, float duration)
        {
            PlayTo(parent, duration, 0f);
        }

        public static void PlayForward(CanvasGroup parent, float duration)
        {
            PlayTo(parent, duration, 1f);
        }
        public static void PlayTo(CanvasGroup parent, float duration, float target)
        {
            PlayTo(parent, duration, target, null);
        }
        public static void PlayTo(CanvasGroup parent, float duration, float target, System.Action OnComplete)
        {
            PlayTo(parent, duration, target, OnComplete, Ease.Unset);
        }
        public static void PlayTo(CanvasGroup parent, float duration, float target, System.Action OnComplete, Ease ease)
        {
            //Add IsThisComponentAnimating component to watch the state of the anima
            DOTween.To(() => parent.alpha, (val) => parent.alpha = val, target, duration).SetEase(ease).SetUpdate(UpdateType.Normal, true).OnComplete(() =>
            {
                OnComplete?.Invoke();
            });
        }
        public static void PlayTo(CanvasGroup parent, float duration, float target, System.Action OnComplete, AnimationCurve ease)
        {
            //Add IsThisComponentAnimating component to watch the state of the anima
            DOTween.To(() => parent.alpha, (val) => parent.alpha = val, target, duration).SetEase(ease).SetUpdate(UpdateType.Normal, true).OnComplete(() =>
            {
                OnComplete?.Invoke();
            });
        }
    }

}
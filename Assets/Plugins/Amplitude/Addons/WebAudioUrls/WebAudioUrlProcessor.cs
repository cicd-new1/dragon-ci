﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace CrazyMinnow.AmplitudeWebGL.WebAudioUrls
{
	/// <summary>
	/// Data for WWW audio clip processing.
	/// </summary>
	public struct AudioUrl
	{
		public readonly string url;
		public readonly AudioType type;

		public AudioUrl(string url, AudioType type)
		{
			this.url = url;
			this.type = type;
		}
	}

	/// <summary>
	/// Notification data for clip processing.
	/// </summary>
	public struct Notification
	{
		public readonly NotificationType type;
		public readonly string clipName;
		public readonly string msg;

		public enum NotificationType
		{
			Info,
			Warning,
			Error
		}

		public Notification(NotificationType type, string clipName, string msg)
		{
			this.type = type;
			this.clipName = clipName;
			this.msg = msg;
		}
	}

	/// <summary>
	/// Place on a GameObject to process and prepare URLs with audio files. URLs will be
	/// asynchronously requested and web-based audio files will be converted to AudioClips
	/// and passed back to the calling delegate (success or failure). If using the queue
	/// processing engine, processed AudioClips will be pushed to another queue for
	/// consumption.
	/// </summary>
	public class WebAudioUrlProcessor : MonoBehaviour {
		public float fetchTimeout = 2.0f; 		// AudioUrl processing timeout, assumes something went wrong fetching AudioUrl.

		/// <summary>
		/// Get the current count of AudioClips available in the queue.
		/// </summary>
		public int QueueCount
		{
			get
			{
				if (clipQueue == null) return 0;
				return clipQueue.Count;
			}
		}

		/// <summary>
		/// Is the URL queue processor running?
		/// </summary>
		public bool IsQueueProcessing
		{
			get
			{
				return (coroQueueProc != null);
			}
		}

		/// <summary>
		/// Callback delegates definitions for successes and failures.
		/// </summary>
		public delegate void SuccessCallback<T>(T response);
		public delegate void FailCallback(Notification msg);

		private SuccessCallback<AudioClip> successCallback;		// cached reference to the success callback
		private FailCallback failCallback;						// cached reference to the fail callback
		private Queue<AudioClip> clipQueue;						// holds AudioClips ready to play
		private Queue<AudioUrl> urlQueue;						// holds enqueued URLs ready to be processed
		private IEnumerator coroQueueProc;						// processing coroutine reference
		private AudioClip cachedClip;							// cached AudioClip, processed and pushed onto queue
		private bool hasError = false;							// web req error flag


		/// <summary>
		/// Returns a processed AudioClip to  the caller if successful (or error message if failed).
		/// Operates asynchronously to handle the www request and linkage.
		/// </summary>
		/// <param name="audioUrl"></param>
		/// <returns></returns>
		private IEnumerator WaitForAudioUrlProcessing(AudioUrl audioUrl)
		{
			// process the AudioUrl (asynchronously)
			yield return ProcessUrl(audioUrl);
			if (!hasError)
				successCallback(cachedClip);	// callback if successful
		}

		/// <summary>
		/// Queue processor: spins through a queue of AudioUrls and creates a queue of
		/// consumable AudioClips.
		/// </summary>
		/// <returns></returns>
		private IEnumerator ProcessUrlQueue()
		{
			while (urlQueue.Count > 0)
			{
				yield return ProcessUrl(urlQueue.Dequeue());
				if (!hasError)
					clipQueue.Enqueue(cachedClip);
			}

			coroQueueProc = null;
		}

		/// <summary>
		/// Accepts an AudioUrl and processes the link/AudioType if possible.
		/// Asynchronous operation waits for www response and clip==ready before
		/// releasing the clip as an AudioClip for consumption.
		/// </summary>
		/// <param name="audioUrl"></param>
		/// <returns></returns>
		private IEnumerator ProcessUrl(AudioUrl audioUrl)
		{
			hasError = false; // assert error condition

			var webRequest = UnityWebRequest.Get(audioUrl.url);
			webRequest.downloadHandler = new DownloadHandlerAudioClip(audioUrl.url, audioUrl.type);

			using (webRequest)
			{
				#if UNITY_2017_2_OR_NEWER // as of 2017.2, UnityWebRequest.Send() becomes obsolete.
				yield return webRequest.SendWebRequest();
				#else
				yield return webRequest.Send();
				#endif

				if (webRequest.isHttpError || webRequest.isNetworkError)
				{
					hasError = true; // controls success callback
					failCallback(new Notification(Notification.NotificationType.Error,
					                              audioUrl.url,
					                              webRequest.error));
				}
				else
				{
					try
					{
						cachedClip = DownloadHandlerAudioClip.GetContent(webRequest);
					}
					catch
					{
						failCallback(new Notification(Notification.NotificationType.Error,
						                              "UnknownException",
						                              "Something did not work correctly, check your URL."));
						hasError = true;
						yield break;
					}

					cachedClip.name = audioUrl.url;

					var timecheck = Time.time;
					while (cachedClip.loadState != AudioDataLoadState.Loaded)
					{
						// wait for (timeout) time to link construct audioclip
						if (Time.time - timecheck > this.fetchTimeout)
						{
							failCallback(new Notification(Notification.NotificationType.Error,
							                              audioUrl.url,
							                              "AudioUrl timeout: " + audioUrl.url + " loadState: " + cachedClip.loadState));
							hasError = true;
							yield break;
						}
						yield return null;
					}
				}
			}
		}

		/// <summary>
		/// Async wait for queue to populate. Uses serialized timeout
		/// value. If the queue gets an item within (timeout) seconds
		/// the value will be returned to the success delegate callback.
		/// Else, a failure delegate callback will be sent.
		/// </summary>
		/// <returns></returns>
		private IEnumerator WaitForQueue()
		{
			var timecheck = Time.time;

			while (Time.time - timecheck < fetchTimeout)
			{
				if (clipQueue.Count > 0)
				{
					successCallback(clipQueue.Dequeue());
					yield break;
				}

				yield return null;
			}

			failCallback(new Notification(Notification.NotificationType.Warning,
			                              "NothingInQueue",
			                              "Queue count ("+clipQueue.Count+") -- Waiting for queue timed out..."));
		}

		/// <summary>
		/// Initialize fail callback delegate, for notification: cannot be null.
		/// </summary>
		/// <param name="failCallback"></param>
		private void SetCallbacks(FailCallback failCallback)
		{
			// check required callbacks, throw errors if null-ref
			if (failCallback == null)
				Debug.LogError("Failure callback not provided: " + this.name + " : " + this.GetType());

			this.failCallback = failCallback;
		}
		/// <summary>
		/// Initialize callback delegates: cannot be null.
		/// </summary>
		/// <param name="successCallback"></param>
		/// <param name="failCallback"></param>
		private void SetCallbacks(SuccessCallback<AudioClip> successCallback, FailCallback failCallback)
		{
			// check required callbacks, throw errors if null-ref
			if (successCallback == null)
				Debug.LogError("Success callback not provided: " + this.name + " : " + this.GetType());

			this.successCallback = successCallback;

			// set the fail callback
			SetCallbacks(failCallback);
		}

		/// <summary>
		/// Helper function: accepts an AudioUrl and callback delegates for success and failure.
		/// </summary>
		/// <param name="audioUrl"></param>
		public void GetAudioClip(AudioUrl audioUrl, SuccessCallback<AudioClip> successCallback, FailCallback failCallback)
		{
			SetCallbacks(successCallback, failCallback);

			// start up the coroutine for async processing of the AudioUrl
			StartCoroutine(WaitForAudioUrlProcessing(audioUrl));
		}

		/// <summary>
		/// Start pushing items to the url queue. If url & clip
		/// queues are not initialized, do so now. If queue
		/// processing coro is not started, spin it up.
		/// </summary>
		/// <param name="audUrl"></param>
		public void EnqueueAudioUrl(AudioUrl audUrl, FailCallback failCallback)
		{
			SetCallbacks(failCallback);

			// if our queues have not been initialized
			if (urlQueue == null)
				urlQueue = new Queue<AudioUrl>();
			if (clipQueue == null)
				clipQueue = new Queue<AudioClip>();

			urlQueue.Enqueue(audUrl);

			// if our queue watcher has not been started
			if (coroQueueProc == null)
			{
				coroQueueProc = ProcessUrlQueue();
				StartCoroutine(coroQueueProc);
			}
		}

		/// <summary>
		/// Asynchronously checks for queued results. If queue has
		/// items, return one. If not, wait for timeout, then fail.
		/// </summary>
		public void GetBatchedAudioClip(SuccessCallback<AudioClip> successCallback, FailCallback failCallback)
		{
			SetCallbacks(successCallback, failCallback);

			// if our clipQueue hasn't been initialized
			if (clipQueue == null)
			{
				failCallback(new Notification(Notification.NotificationType.Warning,
				                              "NoQueue",
				                              "No batch has been previously sent to create a queue."));
				return;
			}

			// if we have AudioClips, return one
			if (clipQueue.Count > 0)
				successCallback(clipQueue.Dequeue());
			else // nothing ready to go, wait for timeout
				StartCoroutine(WaitForQueue());
		}

		/// <summary>
		/// If necessary, clear the clip queue to free up RAM. May be
		/// necessary if the clip queue is no longer necessary.
		/// </summary>
		public void ClearAudioClipQueue()
		{
			if (clipQueue != null)
				clipQueue.Clear();
		}
	}
}

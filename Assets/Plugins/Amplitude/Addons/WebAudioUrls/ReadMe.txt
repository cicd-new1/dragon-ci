RELEASE NOTES:
        0.6.0 : implement UnityWebRequest() vice UnityWebRequestMultimedia(),
                UnityWebRequest.Send() becomes obsolete >=2017.2. Conditional
                directive tested handles: 2017.1, 2017.2, 2017.4, 2018.1,
                2018.2.
		0.5.0 : initial release (beta)

LOCATION OF FILES: Assets\Crazy Minnow Studio\Amplitude\Addons\WebAudioUrls

========================================================================================
INSTRUCTIONS: (visit crazyminnowstudio.com for latest instructions/info)
    There is a demo unitypackage available on the crazyminnowstudio.com download
    section for Amplitude. It contains two additional scripts showing how to
    implement WebAudioUrls for single-action and batch functionality.

    API INSTRUCTIONS:
		public void GetAudioClip(AudioUrl audioUrl, SuccessCallback<AudioClip> successCallback, FailCallback failCallback)
            >> Use this call to send an AudioUrl (composed of a string url and AudioType)
            >> to the processor (WAUP). It will asynchronously be processed and returned
            >> via the supplied SuccessCallback delegate as an AudioClip. The public
            >> field (timeout) can be used to set the maximum time the conversion
            >> attempt will take.

  		public void EnqueueAudioUrl(AudioUrl audUrl, FailCallback failCallback)
            >> Calling this method will send an AudioUrl (composed of a string url and
            >> AudioType) to the processor and queue it up for processing. Multiple
            >> calls may be made in quick succession and the processor will
            >> asynchronously process the AudioUrls into AudioClips (queing them up for
            >> consumption). It will not return anything; however, does require
            >> a FailCallback to be supplied in case of error or difficulty. To
            >> retrieve AudioClips from the queue, use GetBatchedAudioClip (see below).

		public void GetBatchedAudioClip(SuccessCallback<AudioClip> successCallback, FailCallback failCallback)
            >> Companion call to EnqueueAudioUrl (see above). This will fetch the
            >> first AudioClip converted from an enqueued AudioUrl. Each request will
            >> return the oldest AudioClip (first processed) from the queue, sending
            >> it to the SuccessCallback delegate. Once requested, it will be removed
            >> from the queue. You may queue up as many AudioClips as you wish; however,
            >> keep in mind, they will take up memory until consumed and disposed of.

        public void ClearAudioClipQueue()
            >> Clears the WAUP's AudioClip queue. This may be necessary if the queued
            >> AudioClips are no longer needed (i.e. an NPC is removed from the scene
            >> before s/he delivers her/his lines).


PURPOSE:
    This script provides general web-based audio file URL conversion to AudioClip.

KNOWN ISSUES:
    If a bad URL is supplied that does not result in a server error,
    returned data will be fed into the AudioClip. If the current environement is
    Unity Editor (design time), FMOD will throw an exception if it determines
    the data is bad. Confirm the URL is correct and is returning appropriate
    audio data.

========================================================================================
DISCLAIMER: While every attempt has been made to ensure the safe content
	and operation of these files, they are provided as-is, without
	warranty or guarantee of any kind. By downloading and using these
	files you are accepting any and all risks associated and release
	Crazy Minnow Studio, LLC of any and all liability.
========================================================================================

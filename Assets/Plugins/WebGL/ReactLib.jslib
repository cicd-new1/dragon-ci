mergeInto(LibraryManager.library, {
  SignalStartTalking: function () {
    window.dispatchReactUnityEvent("SignalStartTalking");
  },
  SignalEndTalking: function () {
    window.dispatchReactUnityEvent("SignalEndTalking");
  },
  SignalAppLoaded: function () {
    window.dispatchReactUnityEvent("SignalAppLoaded");
  },
});
// Shader targeted for low end devices. Single Pass Forward Rendering.
Shader "FastVRToonShaderURP(With Outline)"
{
    Properties
    {
        [Helpbox(Highly optimized for peak GPU performance on mobile, VR, Switch, and WebGL.)]
        [Helpbox()]
        [Helpbox(Disable unneeded groups to enhance performance.)]
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Surface Type Preset
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Top Level Control)]

        [Tooltip(This is the first option that you want to edit.)]
        [Tooltip(This correlates to the Surface Type in the URP Lit shader.)]
        [Tooltip()]
        [Tooltip(There are 4 first level options,)]
        [Tooltip(.     Opaque means the same as URP Lit shader Opaque.)]
        [Tooltip(.     Opaque(Alpha) means Opaque(but with alpha blending supported). (RenderQueue 2499))]
        [Tooltip(.     Transparent means the same as URP Lit shader Transparent. (RenderQueue 3000))]
        [Tooltip(.     Transparent(ZWrite) means Transparent(but enabled ZWrite and allow Outline). (RenderQueue 3000))]
        [Tooltip()]
        
        [Tooltip(After selecting a preset, the following properties will update their current and default value to match the selected preset.)]
        [Tooltip()]
        [Tooltip(.     _SrcBlend)]
        [Tooltip(.     _DstBlend)]
        [Tooltip(.     _ZWrite)]
        [Tooltip(.     _AlphaClip)]
        [Tooltip(.     _RenderOutline)]
        [Tooltip(.     renderqueue)]
        [Tooltip()]
        [Tooltip(For details about all properties that this preset controls, you can search NiloToonCharacter_SurfaceType_LWGUI_ShaderPropertyPreset inside NiloToonURP folder.)]
        [Preset(NiloToonCharacter_SurfaceType_LWGUI_ShaderPropertyPreset)] _SurfaceTypePreset ("Surface Type", float) = 0 

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Rendering On Off
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Tooltip(.     If enabled, this material will render as usual.)]
        [Tooltip(.     If disabled, this material will still render, but it will not affect any render results (e.g., it becomes invisible).)]
        [Tooltip()]
        [Tooltip(Also, you can turn this toggle on and off to quickly identify where this material is located on a character.)]
        [SubToggle(,_)]_EnableRendering("Render material?", Float) = 1

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Base Map and Color
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Base Map and Color)]
        
        [Tooltip(This color will be multiplied to Base Map (RGBA multiply).)]
        [Tooltip(You can use this color to tint color(RGB) and reduce alpha(A) of Base Map.)]
        [HDR][MainColor]_BaseColor("     Base Color", Color) = (1,1,1,1)
        
        //[AdvancedHeaderProperty]
        [Tooltip(The MainTexture of this material,)]
        [Tooltip(.    RGB channel affects color)]
        [Tooltip(.    A channel affects alpha)]
        [Tooltip()]
        [Tooltip(a.k.a Albedo texture.)]
        [MainTexture]_BaseMap("     Base Map", 2D) = "white" {} // Not using [Tex], in order to preserve tiling and offset GUI

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Core Settings)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Render Face(Cull)
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Tooltip(Similar to the Render Face of URP Lit shader. Decide which polygon face to render, it is not related to IsFace settings.)]
        [Main(_RenderFaceGroup,_,off,off)]_RenderFaceGroup("Render Face(Cull)", Float) = 0

        [Helpbox(Please set Preset to Both for double face material, usually for displaying inner polygons of cloths like skirt.)]
        [Helpbox((High performance cost only if using Both))]
        
        [AdvancedHeaderProperty]
        [Title(_RenderFaceGroup,Preset)]
        [Tooltip(Unless the mesh has a wrong polygon facing, you only need to use Front or Both, and you can ignore Back and Both(Flip).)]
        [Tooltip()]
        [Tooltip(This preset controls the following params,)]
        [Tooltip(.     Lit pass (_Cull))]
        [Tooltip(.     Outline pass (_CullOutline))]
        [Tooltip(.     NiloToonSelfShadowCaster pass (_CullNiloToonSelfShadowCaster))]
        [Preset(_RenderFaceGroup,NiloToonCharacter_RenderFace_LWGUI_ShaderPropertyPreset)] _RenderFacePreset ("Preset", float) = 0 

        [Advanced]
        [Title(_RenderFaceGroup,. Render Face)]
        // https://docs.unity3d.com/ScriptReference/Rendering.CullMode.html
        [Tooltip(Same as URP Lit shader Render Face.)]
        [Tooltip()]
        [Tooltip(Use this dropdown to determine which sides of your geometry to render.)]
        [Tooltip()]
        [Tooltip(Options are,)]
        [Tooltip(.    Front    . Renders the front face of your geometry and culls the back face. This is the default setting.)]
        [Tooltip(.    Back     . Renders the back face of your geometry and culls the front face.)]
        [Tooltip(.    Both     . Renders both faces of the geometry, this is good for flat objects with single face polygon, like cloth and hair, where you might want both sides visible.)]
        [SubEnum(_RenderFaceGroup,Front,2,Back,1,Both,0)]_Cull("  Lit pass", Float) = 2

        [Advanced]
        [Tooltip(Use this dropdown to determine which sides of the outline geometry to render.)]
        [Tooltip(Outline pass should always renders the opposite face of surface color pass)]
        [Tooltip()]
        [Tooltip(Options are,)]
        [Tooltip(.    Front    . Renders the front face of your geometry and culls the back face.)]
        [Tooltip(.    Back     . Renders the back face of your geometry and culls the front face. This is the default setting.)]
        [SubEnum(_RenderFaceGroup,Front,2,Back,1)]_CullOutline("  Outline pass", Float) = 1
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Classic Outline
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Tooltip(Enable to redraw the back face of the mesh again, and adding a little vertex extrusion along normal or smoothed normal direction, which makes this redraw looks like an outline.)]
        [Tooltip(This outline method does not require any postprocessing or screen space render pass, so performance is usually good, unless vertex count is too high.)]
        [Tooltip()]
        [Tooltip(We call this Classic Outline, since it is the fastest and mostly used realtime outline method to draw character outline since the year 2000(Jet Set Radio).)] 
        [Tooltip(Many realtime application and games use this classic outline method also (e.g. Guilty Gear Xrd, Genshin Impact, VRM MToon))]
        [Tooltip()]
        [Tooltip(NiloToon improve this outline method by using an auto generated smoothed normal for extrude direction instead of lighting normal, and adding outline Z Offset, which will produce a much better outline result.)]
        [PassSwitch(NiloToonLiteClassicOutline)][Main(_RENDER_OUTLINE,_)]_RenderOutline("Classic Outline", Float) = 1
        
        [Helpbox(When enabled, this material will produce a classic outline by redrawing the mesh with inverted culling and a slight extrusion based on the outline width.)]
        [Helpbox((Very High performance cost))]
        
        [Title(_RENDER_OUTLINE, Width)]
        [Tooltip(The width of outline, usually it is between 0.25 to 1.0.)]
        [Tooltip()]
        [Tooltip(If you convert a material from VRM MToon to NiloToon, NiloToon will keep MToon outline width here. Then you should keep this Width untouched, and edit Width(extra) instead. This way you can preserve MToon material outline width.)]
        [Sub(_RENDER_OUTLINE)]_OutlineWidth("Width", Range(0,32)) = 0.6
        [Tooltip(Just like the Width mentioned above, this width serves as an additional multiplier control for convenience.)]
        [Tooltip(Sometimes when you convert a material from another shader (e.g. VRM MToon) to NiloToon, that shader already stored outline width in material(_OutlineWidth), then you can use this slider to do any extra width multiplier control without editing the above Width(_OutlineWidth).)]
        [Tooltip(.    For VRM0.x MToon converted materials, 12 to 18 is a good starting point)]
        [Tooltip(.    For VRM1.0 MToon converted materials, 512 is a good starting point)]
        [Sub(_RENDER_OUTLINE)]_OutlineWidthExtraMultiplier("Width(extra)", Range(0,2048)) = 1
        // TODO: port width tex?
        // TODO: port width vertex color?
        
        [Title(_RENDER_OUTLINE, ........................................................................................................................................................................................................................................................................................................................................................................)]

        [Title(_RENDER_OUTLINE, Color)]
        [Tooltip(The resulting outline color is derived from the Base Map multiplied by this outline Tint Color.)]
        [Sub(_RENDER_OUTLINE)][HDR]_OutlineTintColor("Tint Color", Color) = (0.25,0.25,0.25,1)
        
        [Title(_RENDER_OUTLINE, ........................................................................................................................................................................................................................................................................................................................................................................)]

        [Title(_RENDER_OUTLINE, Base Z Offset)]
        [Tooltip(Outline Z Offset is used to hide inner outline. The higher the outline Z Offset, the less inner outline can appear.)]
        [Tooltip(Increasing this a bit can hide most of the ugly inner outline.)]
        [Tooltip()]
        [Tooltip(Outline Z Offset is the View Space unit that pushs outline away from camera (1 Z Offset is 1m).)]
        [Tooltip()]
        [Tooltip(Outline Z Offset will only modify clip space position.z of the outline vertex, so modify Z Offset will not change the outline vertex XY position on screen, it will only affects depth buffer Z value (affects per pixel depth sorting).)]
        [Tooltip()]
        [Tooltip(This is the Base Z Offset, so it will NOT be affected by any Z Offset Mask.)]
        [Sub(_RENDER_OUTLINE)]_OutlineBaseZOffset("Base Z Offset", Range(0,1)) = 0
        
        [Title(_RENDER_OUTLINE, ........................................................................................................................................................................................................................................................................................................................................................................)]
        [Title(_RENDER_OUTLINE, Maskable Z Offset)]
        [Tooltip(Outline Z Offset is used to hide inner outline. The higher the outline Z Offset, the less inner outline can appear.)]
        [Tooltip(Increasing this a bit can hide most of the ugly inner outline, so the default value is not 0.)]
        [Tooltip()]
        [Tooltip(Outline Z Offset is the View Space unit that pushs outline away from camera (1 Z Offset is 1m).)]
        [Tooltip()]
        [Tooltip(Outline Z Offset will only modify clip space position.z of the outline vertex, so modify Z Offset will not change the outline vertex XY position on screen, it will only affects depth buffer Z value (affects per pixel depth sorting).)]
        [Tooltip()]
        [Tooltip(This is a Maskable Z Offset, so it will be affected by Z Offset Mask.)]
        [Sub(_RENDER_OUTLINE)]_OutlineZOffset("Z Offset (increase for face!)", Range(0,1)) = 0.0001
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // RimLight 3D (no need to sync with NiloToon)
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Main(_RimLightGroup,_)]_EnableRimLight("RimLight 3D", Float) = 1 
        [Helpbox(Vertex quality fast rim light. Different to the expensive 2D RimLight of the Standard NiloToon Character shader.)]
        [Helpbox((No performance cost))]
        [Sub(_RimLightGroup)][HDR]_RimColor("Color", Color) = (4,4,4)
        [Sub(_RimLightGroup)]_RimThreshold("Threshold", Range(0,1)) = 0.6
        [Sub(_RimLightGroup)]_RimSoftness("Softness", Range(0,0.5)) = 0.05
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Ramp Tex
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Main(_RampTexGroup,_RAMPTEX_ON)]_UseRampTex("Ramp Tex", Float) = 0
        
        [Helpbox(Define shadow result by a custom ramp texture)]
        [Helpbox((Very Low performance cost))]
        [Ramp(_RampTexGroup, RampMap, Assets, 256)][NoScaleOffset]_LightingRampTex("RampTex", 2D) = "white" {}
        [Sub(_RampTexGroup)]_LightingRampSamplingOffset("    Offset", Range(-1,1)) = 0
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Render States)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Stencil Buffer
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: can we support stencil if Extra thick outline is needed?
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Color buffer
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //[ShowIf(_UIDisplayMode, GEqual, 200)]
        [Tooltip(Controls Render States related to color buffer, like Blending and ColorMask.)]
        [Main(_ColorRenderStatesGroup,_,off,off)]_ColorRenderStatesGroup("Color Buffer", Float) = 0

        [Title(_ColorRenderStatesGroup,Blending State)]
        // https://docs.unity3d.com/ScriptReference/Rendering.BlendMode.html
        // https://docs.unity3d.com/Manual/SL-Blend.html
        // Source Factor
        [Tooltip(In most cases, you do NOT need to edit this, since selecting a Surface Type Preset will handle this for you already.)]
        [Tooltip()]
        [Tooltip(Determines how the GPU combines the output of the fragment shader with the render target.)]
        [Tooltip(This section will only affect ForwardLit color pass.)]
        [Tooltip()]
        [Tooltip(For example, you can use)]
        [Tooltip(.    One,Zero for Opaque.)]
        [Tooltip(.    SrcAlpha,OneMinusSrcAlpha for Semi Transparency.)]
        [Tooltip(.    One,One for Additive.)]
        [SubEnumDrawer(_ColorRenderStatesGroup,UnityEngine.Rendering.BlendMode)]_SrcBlend("Source Factor", Float) = 1
        // Destination Factor
        [Tooltip(In most cases, you do NOT need to edit this, since selecting a Surface Type Preset will handle this for you already.)]
        [Tooltip()]
        [Tooltip(Determines how the GPU combines the output of the fragment shader with the render target.)]
        [Tooltip(This section will only affect ForwardLit color pass.)]
        [Tooltip()]
        [Tooltip(For example, you can use)]
        [Tooltip(.    One,Zero for Opaque.)]
        [Tooltip(.    SrcAlpha,OneMinusSrcAlpha for Semi Transparency.)]
        [Tooltip(.    One,One for Additive.)]
        [SubEnumDrawer(_ColorRenderStatesGroup,UnityEngine.Rendering.BlendMode)]_DstBlend("Destination Factor", Float) = 0
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Depth Buffer
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //[ShowIf(_UIDisplayMode, GEqual, 200)]
        [Tooltip(Controls Render States related to depth buffer, like ZWrite and ZTest.)]
        [Main(_DepthRenderStatesGroup,_,off,off)]_DepthRenderStatesGroup("Depth Buffer", Float) = 0

        [Title(_DepthRenderStatesGroup,ZWrite)]
        // https://docs.unity3d.com/Manual/SL-ZWrite.html
        [Tooltip(In most cases, you do NOT need to edit this, since selecting a Surface Type Preset will handle this for you already.)]
        [Tooltip()]
        [Tooltip(Sets whether the depth buffer contents are updated during rendering.)]
        [Tooltip()]
        [Tooltip(.    If enabled, this material writes into the depth buffer.)]
        [Tooltip(.    If disabled, this material does NOT write into the depth buffer.)]
        [Tooltip()]
        [Tooltip(Changing Surface Type Preset will turn this off if the selected preset is Transparent (without ZWrite).)]
        [SubToggle(_DepthRenderStatesGroup,_)]_ZWrite("Write to depth buffer?", Float) = 1

        [Title(_DepthRenderStatesGroup,ZTest)]
        // https://docs.unity3d.com/Manual/SL-ZTest.html
        // https://docs.unity3d.com/ScriptReference/Rendering.CompareFunction.html
        [Tooltip(In most cases, you do NOT need to edit this, but we still provide ZTest control in case you need it.)]
        [Tooltip()]
        [Tooltip(Sets the conditions under which geometry passes or fails depth testing.)]
        [Tooltip()]
        [Tooltip(Default LessEqual.)] // LWGUI can't show the correct default value, so we write it again.
        [SubEnumDrawer(_DepthRenderStatesGroup,UnityEngine.Rendering.CompareFunction)]_ZTest("Depth test pass condition", Float) = 4 // 4 is LEqual
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Edit Alpha channel and Alpha Clipping)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // BaseMap Alpha Modify
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: do we need this?
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Alpha Clipping
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Tooltip(Same as the Alpha Clipping of URP Lit shader.)]
        [Tooltip()]
        [Tooltip(Enable this will make your Material act like a Cutout Shader. Use this to create a transparent effect with hard edges between the opaque and transparent areas.)]
        [Tooltip()]
        [Tooltip(For example, adding lace holes or cutout to an Opaque cloth material.)]
        [Main(_AlphaClippingGroup,_ALPHATEST_ON)]_AlphaClip("Alpha Clipping", Float) = 0

        [Helpbox(Same as the Alpha Clipping of URP Lit shader.)]
        [Helpbox((Very High performance cost))]
        
        [Title(_AlphaClippingGroup, Cutout Threshold)]
        [Tooltip(All pixels above your threshold will render as usual, and all pixels below your threshold are invisible.)]
        [Tooltip(For example, a threshold of 0.1 means that this material does not render when alpha values of that pixel is below 0.1.)]
        [Sub(_AlphaClippingGroup)]_Cutoff("Threshold", Range(0.0, 1.0)) = 0.5
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Override Alpha Output
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: do we need this?

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Offset vertex Z sorting)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Z Offset (eyebrow)
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
        [Tooltip(Offset vertex shader Clip space depth value (SV_POSITION.z).)] 
        [Tooltip(Useful for pushing eyebrow on top of hair or pushing special face expression over cheek.)]
        [Main(_ZOffsetGroup,_)]_ZOffsetEnable("Z Offset (eyebrow)", Float) = 0

        [Helpbox(Usually for pushing eyebrow on top of the hair.)]
        [Helpbox((No performance cost))]
        
        [Title(_ZOffsetGroup, ZOffset)]
        [Tooltip(Offset vertex shader Clip space depth value (SV_POSITION.z), 1 ZOffset is 1m in view space.)]
        [Tooltip(The slider range is 2m, which should be more than enough for character.)] 
        [Tooltip(Z Offset will only modify clip space position.z of the vertex, so modify Z Offset will not change the vertex XY position on screen, it will only affect depth buffer Z value (affects per pixel depth sorting).)]
        [Tooltip()]
        [Tooltip(Negative 0.025 is a good starting value for eyebrow rendering on top of hair.)]
        [Sub(_ZOffsetGroup)]_ZOffset("Z Offset", Range(-2,2)) = 0.0
        
        /*
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Replacing BaseMap Result)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // MatCap (Color Replace)
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: add support?
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Adding extra local Shadow)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Occlusion Map
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: add support?
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // MatCap (Shadow)
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: add support?
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Receiving ShadowMap Shadow)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // URP Directional Light ShadowMap
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: add support?
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Adding Specular Reflection Highlight)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // MatCap (Add/Specular/RimLight)
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: add support?
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Adding Self Glow Emission)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Emission
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // TODO: add support?
        */
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Per Material Effects)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Per Material Dither Fade
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Main(_PerMaterialDitherFadeGroup,_PER_MAT_DITHER_FADE_ON)]_EnablePerMaterialDitherFade("Dither Fade", Float) = 0
        [Helpbox((Extremely High performance cost when amount is not 1))]
        [Sub(_PerMaterialDitherFadeGroup)]_PerMaterialDitherFadeAmount("Amount", Range(0.0, 1.0)) = 0
        
        // TODO: finish this feature
        /*
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Per Material Dissolve
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Main(_PerMaterialDissolveGroup,_PER_MAT_DISSOLVE_ON)]_EnablePerMaterialDissolve("Dissolve", Float) = 0
        [Helpbox((Extremely High performance cost when threshold is not 1))]
        [Sub(_PerMaterialDissolveGroup)]_PerMaterialDissolveCutoff("Threshold", Range(0.0, 1.0)) = 0
        */
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Per Material Extra Thick Outline
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [PassSwitch(NiloToonLiteExtraThickOutline)][Main(_PerMaterialExtraThickOutlineGroup,_)]_EnablePerMaterialExtraThickOutline("Extra Thick Outline", Float) = 0
        
        [Helpbox(When enabled, this material will produce an extra thick outline by redrawing the mesh with inverted culling and a slight extrusion based on the outline width.)]
        [Helpbox((Very High performance cost))]
        
        [Title(_PerMaterialExtraThickOutlineGroup, Width)]
        [Tooltip(The width of extra thick outline, usually it is between 0.25 to 1.0.)]
        [Tooltip()]
        [Sub(_PerMaterialExtraThickOutlineGroup)]_PerMaterialExtraThickOutlineWidth("Width", Range(0,32)) = 2
        
        [Title(_PerMaterialExtraThickOutlineGroup, Color)]
        [Tooltip(The resulting outline color is derived from the Base Map multiplied by this outline Tint Color.)]
        [Sub(_PerMaterialExtraThickOutlineGroup)][HDR]_PerMaterialExtraThickOutlineTintColor("Tint Color", Color) = (1,1,1,1)
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Pass On Off)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Allowed Passes
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Main(_PassOnOffGroup,_,off,off)]_PassOnOffGroup("Allowed Pass", Float) = 0

        [Helpbox(Disable unneeded passes to enhance performance.)]
        
        [PassSwitch(UniversalForwardOnly)][SubToggle(_PassOnOffGroup,_)]_AllowRenderUniversalForwardOnlyPass("UniversalForwardOnly", Float) = 1
        [PassSwitch(ShadowCaster)][SubToggle(_PassOnOffGroup,_)]_AllowRenderShadowCasterPass("ShadowCaster", Float) = 1
        [PassSwitch(DepthOnly)][SubToggle(_PassOnOffGroup,_)]_AllowRenderDepthOnlyPass("DepthOnly", Float) = 1
        [PassSwitch(DepthNormals)][SubToggle(_PassOnOffGroup,_)]_AllowRenderDepthNormalsPass("DepthNormals", Float) = 1
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Title(Other)]
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Fog
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Main(_FogGroup,_ENABLE_FOG)]_EnableFog("Fog", Float) = 1

        // ObsoleteProperties
        [HideInInspector] _MainTex("BaseMap", 2D) = "white" {}
        [HideInInspector] _Color("Base Color", Color) = (1, 1, 1, 1)
        [HideInInspector] _Shininess("Smoothness", Float) = 0.0
        [HideInInspector] _GlossinessSource("GlossinessSource", Float) = 0.0
        [HideInInspector] _SpecSource("SpecularHighlights", Float) = 0.0

        [HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}
    }

    SubShader
    {       
        Tags 
        {
            "RenderType" = "Opaque"
            "RenderPipeline" = "UniversalPipeline"
            "IgnoreProjector" = "True"
        }
    
        HLSLINCLUDE
        
        #pragma shader_feature_local _ALPHATEST_ON
        #pragma shader_feature_local _DISSOLVE_ON
        #pragma shader_feature_local _PER_MAT_DITHER_FADE_ON
        
        ENDHLSL

        Pass
        {               
            Name "ForwardLit"
            Tags
            {
                "LightMode" = "UniversalForwardOnly"
            }

            // -------------------------------------
            // Render state (URP's ComplexLit.shader)
            Blend [_SrcBlend] [_DstBlend] , One OneMinusSrcAlpha // Alpha Op set to "One OneMinusSrcAlpha" (or "OneMinusDstAlpha One" is also correct), so semi-transparent material can update RenderTarget's a correctly, see https://twitter.com/MuRo_CG/status/1511543317883863045
            ZWrite [_ZWrite]
            Cull [_Cull]

            // Render state (NiloToon extra added)
            //ColorMask [_ColorMask]
            //ZTest [_ZTest]
            
            Stencil
            {
                Ref 147
                Comp Always
                Pass Replace
            }
            
            HLSLPROGRAM

            // ---------------------------------------------------------------------------------------------
            // Material keywords
            #pragma shader_feature_local _ENABLE_FOG
            #pragma shader_feature_local _RAMPTEX_ON
            
            // ---------------------------------------------------------------------------------------------
            // Universal Pipeline keywords (you can always reference this section from URP's ComplexLit.shader)
            #pragma multi_compile_fog
            // ---------------------------------------------------------------------------------------------

            #define FastURPToonLitColorPass 1
            #include "FastVRToonShaderURP_Shared.hlsl"

            #pragma vertex VertexShaderWork
            #pragma fragment ShadeFinalColor

            ENDHLSL
        }

        Pass 
        {
            Name "Outline"
            Tags 
            {
               "LightMode" = "NiloToonLiteClassicOutline"
            }

            // Render state (URP's ComplexLit.shader)
            //Blend [_SrcBlend] [_DstBlend] , One OneMinusSrcAlpha // Alpha Op set to "One OneMinusSrcAlpha" (or "OneMinusDstAlpha One" is also correct), so semi-transparent material can update RenderTarget's a correctly, see https://twitter.com/MuRo_CG/status/1511543317883863045
            //ZWrite [_ZWrite]
            Cull [_CullOutline]

            // Render state (NiloToon extra added)
            //ColorMask [_ColorMask]
            //ZTest [_ZTest]
            
            //Stencil{}
            
            HLSLPROGRAM

            // ---------------------------------------------------------------------------------------------
            // Material keywords
            #pragma shader_feature_local _ENABLE_FOG
            #pragma shader_feature_local _RAMPTEX_ON
            
            // ---------------------------------------------------------------------------------------------
            // Universal Pipeline keywords (you can always reference this section from URP's ComplexLit.shader)
            #pragma multi_compile_fog
            // ---------------------------------------------------------------------------------------------

            #define FastURPToonLitOutlinePass 1
            #include "FastVRToonShaderURP_Shared.hlsl"

            #pragma vertex VertexShaderWork
            #pragma fragment ShadeFinalColor

            ENDHLSL
        }

        Pass 
        {
            Name "ExtraThickOutline"
            Tags 
            {
               "LightMode" = "NiloToonLiteExtraThickOutline"
            }

            // Render state (URP's ComplexLit.shader)
            Blend SrcAlpha OneMinusSrcAlpha , One OneMinusSrcAlpha // Alpha Op set to "One OneMinusSrcAlpha" (or "OneMinusDstAlpha One" is also correct), so semi-transparent material can update RenderTarget's a correctly, see https://twitter.com/MuRo_CG/status/1511543317883863045
            //ZWrite [_ZWrite]
            Cull [_CullOutline]

            // Render state (NiloToon extra added)
            //ColorMask [_ColorMask]
            //ZTest [_ZTest]
            
            Stencil
            {
                Ref 147
                Comp NotEqual
            }
            
            HLSLPROGRAM

            // ---------------------------------------------------------------------------------------------
            // Material keywords
            // ...
            
            // ---------------------------------------------------------------------------------------------
            // Universal Pipeline keywords (you can always reference this section from URP's ComplexLit.shader)
            //#pragma multi_compile_fog
            // ---------------------------------------------------------------------------------------------

            #define FastURPToonLitExtraThickOutlinePass 1
            #include "FastVRToonShaderURP_Shared.hlsl"

            #pragma vertex VertexShaderWork
            #pragma fragment FragExtraThickOutline

            ENDHLSL
        }
 
        Pass
        {
            Name "ShadowCaster"
            Tags
            {
                "LightMode" = "ShadowCaster"
            }

            // -------------------------------------
            // Render State Commands
            ZWrite On
            ZTest LEqual
            ColorMask 0
            Cull[_Cull]

            HLSLPROGRAM

            // This is used during shadow map generation to differentiate between directional and punctual light shadows, as they use different formulas to apply Normal Bias
            #pragma multi_compile_vertex _ _CASTING_PUNCTUAL_LIGHT_SHADOW
            
            #include "FastVRToonShaderURP_Shared.hlsl"

            #pragma vertex VertexShaderWork
            #pragma fragment FragClipOnly

            ENDHLSL
        }

        Pass
        {
            Name "DepthOnly"
            Tags
            {
                "LightMode" = "DepthOnly"
            }

            // -------------------------------------
            // Render State Commands
            ZWrite On
            ColorMask R
            Cull[_Cull]

            HLSLPROGRAM

            // -------------------------------------
            // Shader Stages
            #pragma vertex VertexShaderWork
            #pragma fragment FragClipOnly

            // -------------------------------------
            // Material Keywords

            // -------------------------------------
            // Unity defined keywords

            //--------------------------------------
            // GPU Instancing

            // -------------------------------------
            // Includes
            #include "FastVRToonShaderURP_Shared.hlsl"
            
            ENDHLSL
        }

        // This pass is used when drawing to a _CameraNormalsTexture texture
        Pass
        {
            Name "DepthNormals"
            Tags
            {
                "LightMode" = "DepthNormals"
            }

            // -------------------------------------
            // Render State Commands
            ZWrite On
            Cull[_Cull]

            HLSLPROGRAM

            // -------------------------------------
            // Shader Stages
            #pragma vertex VertexShaderWork
            #pragma fragment FragClipOnly

            // -------------------------------------
            // Material Keywords

            // -------------------------------------
            // Unity defined keywords
            // Universal Pipeline keywords

            // -------------------------------------
            #include "FastVRToonShaderURP_Shared.hlsl"
            
            ENDHLSL
        }
    }
    CustomEditor "LWGUI.LWGUI"

    FallBack "Hidden/Universal Render Pipeline/FallbackError"
}

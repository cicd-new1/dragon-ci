#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

#include "NiloOutlineUtil.hlsl"
#include "NiloZOffsetUtil.hlsl"
#include "NiloDitherFadeoutClipUtil.hlsl"
#include "NiloInvLerpRemapUtil.hlsl"

#if _ENABLE_FOG && (defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2))
    #define NeedFog 1
#endif

struct Attributes
{
    float3 positionOS   : POSITION;
    float3 normalOS     : NORMAL;
    float2 uv           : TEXCOORD0;
};

struct Varyings
{
    float4 positionCS               : SV_POSITION;

    // TODO: conditionally pack matcap uv using #if AnyMatCap
    float2 uv                       : TEXCOORD0;

#if NeedFog  
    half3 rampUvU_Rim_FogFactor    : TEXCOORD1;
#else
    half2 rampUvU_Rim_FogFactor    : TEXCOORD1;
#endif
};

///////////////////////////////////////////////////////////////////////////////////////
// CBUFFER and Uniforms 
///////////////////////////////////////////////////////////////////////////////////////

sampler2D _BaseMap; 
TEXTURE2D(_LightingRampTex);

// if required, you can disabled SRP-batching to avoid SRP-batching bug in android / VR
CBUFFER_START(UnityPerMaterial)

    // Render material?
    float   _EnableRendering;

    // base color
    half4   _BaseColor; 
    float4  _BaseMap_ST;

    // alpha clipping
    half    _Cutoff;

    // ramp
    half    _LightingRampSamplingOffset;

    // ZOffset
    float   _ZOffsetEnable;
    float   _ZOffset;

    // outline
    float   _OutlineWidth;
    float   _OutlineWidthExtraMultiplier;
    half3   _OutlineTintColor;
    float   _OutlineBaseZOffset;
    float   _OutlineZOffset;

    // rim light
    half    _EnableRimLight;
    half3   _RimColor;
    half    _RimThreshold;
    half    _RimSoftness;

    // per material dither fadeout
    half    _PerMaterialDitherFadeAmount;

    // extra thick outline
    float   _PerMaterialExtraThickOutlineWidth;
    half4   _PerMaterialExtraThickOutlineTintColor;
CBUFFER_END

struct ToonSurfaceData
{
    half4   albedoWithAlpha;
    // TODO: emission
};

///////////////////////////////////////////////////////////////////////////////////////
// vertex shared functions
///////////////////////////////////////////////////////////////////////////////////////

float3 TransformPositionWSToOutlinePositionWS(float3 positionWS, float positionVS_Z, float3 normalWS)
{
    float outlineWidth = _OutlineWidth * _OutlineWidthExtraMultiplier;
    
#if FastURPToonLitExtraThickOutlinePass
    outlineWidth += _PerMaterialExtraThickOutlineWidth;
#endif
    
    float outlineExpandAmount = outlineWidth * GetOutlineCameraFovAndDistanceFixMultiplier(positionVS_Z);
    return positionWS + normalWS * outlineExpandAmount; 
}

Varyings VertexShaderWork(Attributes input)
{
    Varyings output;

    // (_PerMaterialDitherFadeAmount == 1) is an optimization
    if(!_EnableRendering
#if _PER_MAT_DITHER_FADE_ON
        || _PerMaterialDitherFadeAmount >= 1
#endif
        )
    {
        return (Varyings)0;
    }
    
    VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS);
    VertexNormalInputs vertexNormalInput = GetVertexNormalInputs(input.normalOS);

    float3 positionWS = vertexInput.positionWS;

#if FastURPToonLitOutlinePass || FastURPToonLitExtraThickOutlinePass
    positionWS = TransformPositionWSToOutlinePositionWS(vertexInput.positionWS, vertexInput.positionVS.z, vertexNormalInput.normalWS);
#endif

    /////////////////////////////////////////
    // UV
    /////////////////////////////////////////   
    output.uv = TRANSFORM_TEX(input.uv,_BaseMap);

    /////////////////////////////////////////
    // ZOffset sum  
    /////////////////////////////////////////
    float ZOffsetFinalSum = 0;

    // ZOffset
    ZOffsetFinalSum += -_ZOffset * _ZOffsetEnable;

    // Outline ZOffset
#if FastURPToonLitOutlinePass
    ZOffsetFinalSum += -(_OutlineBaseZOffset+_OutlineZOffset);
#endif
    
    /////////////////////////////////////////
    // positionCS
    /////////////////////////////////////////   
    output.positionCS = TransformWorldToHClip(positionWS);

    output.positionCS = NiloGetNewClipPosWithZOffsetVS(output.positionCS, ZOffsetFinalSum);
    
    Light mainLight = GetMainLight();       
    half halfLambertNoL = dot(vertexNormalInput.normalWS, mainLight.direction) * 0.5 + 0.5;

    /////////////////////////////////////////
    // rampUvU
    /////////////////////////////////////////
    output.rampUvU_Rim_FogFactor.x = saturate(halfLambertNoL + _LightingRampSamplingOffset);

    /////////////////////////////////////////
    // fog
    /////////////////////////////////////////
#if NeedFog
    output.rampUvU_Rim_FogFactor.z = ComputeFogFactor(vertexInput.positionCS.z);
#endif

    /////////////////////////////////////////
    // Rim light
    /////////////////////////////////////////
    half3 viewDirWS = normalize(GetWorldSpaceViewDir(positionWS));
    half NoV = saturate(1-dot(vertexNormalInput.normalWS,viewDirWS)) * halfLambertNoL;
    output.rampUvU_Rim_FogFactor.y = _EnableRimLight? NoV : 0;

    return output;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// fragment shared functions (important to optimize this section as much as possible, for VR)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void DoClipTestToTargetAlphaValue(half alpha) 
{
#if _ALPHATEST_ON
    clip(alpha - _Cutoff);
#endif
}
void DoDissolve(inout ToonSurfaceData data, Varyings input)
{
#if _DISSOLVE_ON
    // sample map
    // clip
#endif
}
void DoDitherFadeoutClip(Varyings input)
{
    // dither fade should affect URP's shadowmap and depth texture / depth normal texture also
#if _PER_MAT_DITHER_FADE_ON
    NiloDoDitherFadeoutClip(input.positionCS.xy, 1.0-_PerMaterialDitherFadeAmount);
#endif   
}
ToonSurfaceData InitializeSurfaceData(Varyings input)
{
    ToonSurfaceData output;

    output.albedoWithAlpha = tex2D(_BaseMap, input.uv) * _BaseColor;

    // once alpha is finalized, process all clip() first to early exit
    DoClipTestToTargetAlphaValue(output.albedoWithAlpha.a);

    DoDissolve(output, input);

    DoDitherFadeoutClip(input);

    // TODO:
    // - emission
    // - occlusion
    // - normal map
    // - matcap replace
    
    return output;
}

half3 ShadeAllLights(ToonSurfaceData surfaceData, Varyings input)
{
    // sample ramp result, read the texture asap, but use the result as late as possible for latency hiding
#if _RAMPTEX_ON
    const half3 rampTint = SAMPLE_TEXTURE2D_LOD(_LightingRampTex, sampler_LinearClamp, half2(input.rampUvU_Rim_FogFactor.x,0),0).rgb;
#endif
    
    const Light mainLight = GetMainLight();

    // tint clamped light color
    // give a minimum since we don't sample indirect light
    half3 result = saturate(max(0.25,mainLight.color));

    // rim light
#if FastURPToonLitColorPass
    result += invLerpClamp(_RimThreshold - _RimSoftness,_RimThreshold, input.rampUvU_Rim_FogFactor.y) * _RimColor * mainLight.color;
#endif

    result *= surfaceData.albedoWithAlpha.rgb;

#if _RAMPTEX_ON
    result *= rampTint;
#endif
    
    return result;
}

void ConvertToOutlineColor(inout half3 inputColor)
{
#if FastURPToonLitOutlinePass
    inputColor *= _OutlineTintColor;
#endif
}

void ApplyFog(inout half3 inputColor, Varyings input)
{
#if NeedFog
    inputColor = MixFog(inputColor, input.rampUvU_Rim_FogFactor.z);
#endif   
}

half4 ShadeFinalColor(Varyings input) : SV_TARGET
{
    ToonSurfaceData surfaceData = InitializeSurfaceData(input);
    
    half3 result = ShadeAllLights(surfaceData, input);
    
    ConvertToOutlineColor(result);
    ApplyFog(result, input);

    return half4(result,surfaceData.albedoWithAlpha.a);
}

half4 FragExtraThickOutline(Varyings input) : SV_TARGET
{
    return _PerMaterialExtraThickOutlineTintColor;
}

half4 FragClipOnly(Varyings input) : SV_TARGET
{
    ToonSurfaceData surfaceData = InitializeSurfaceData(input);

    return 0;
}


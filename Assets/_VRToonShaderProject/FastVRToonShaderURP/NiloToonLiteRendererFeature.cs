using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.Rendering.Universal.Internal;

public class NiloToonLiteRendererFeature : ScriptableRendererFeature
{
    RenderObjectsPass m_OpaqueQueueClassicOutlinePass;
    RenderObjectsPass m_TransparentQueueClassicOutlinePass;
    RenderObjectsPass m_ExtraThickOutlinePass;

    /// <inheritdoc/>
    public override void Create()
    {
        m_OpaqueQueueClassicOutlinePass = new RenderObjectsPass(
            "NiloToonLiteClassicOutline-OpaqueQueue",
            RenderPassEvent.BeforeRenderingSkybox,
            new string[] { new string("NiloToonLiteClassicOutline") },
            RenderQueueType.Opaque,
            int.MaxValue,
            new RenderObjects.CustomCameraSettings()
        );
        m_TransparentQueueClassicOutlinePass = new RenderObjectsPass(
            "NiloToonLiteClassicOutline-TransparentQueue",
            RenderPassEvent.AfterRenderingTransparents,
            new string[] { new string("NiloToonLiteClassicOutline") },
            RenderQueueType.Transparent,
            int.MaxValue,
            new RenderObjects.CustomCameraSettings()
        );
        m_ExtraThickOutlinePass = new RenderObjectsPass(
            "NiloToonLiteExtraThickOutline",
            RenderPassEvent.AfterRenderingTransparents + 1,
            new string[] { new string("NiloToonLiteExtraThickOutline") },
            RenderQueueType.Opaque,
            int.MaxValue,
            new RenderObjects.CustomCameraSettings()
        );
    }

    // Here you can inject one or multiple render passes in the renderer.
    // This method is called when setting up the renderer once per-camera.
    public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
    {
        renderer.EnqueuePass(m_OpaqueQueueClassicOutlinePass);
        renderer.EnqueuePass(m_TransparentQueueClassicOutlinePass);
        renderer.EnqueuePass(m_ExtraThickOutlinePass);
    }
}


